! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module visit
  use parallel
  use variables
  use grid
  use basis
  
  implicit none
  !include "silo.inc"
  include "silo_f9x.inc"
  
  ! Time info
  integer :: nout_time

  ! Write count
  integer :: visit_count
  
  ! Output variables
  integer :: output_n,output_max
  character(len=str_medium), dimension(:),   allocatable :: output_types
  character(len=str_medium), dimension(:),   allocatable :: output_names
  character(len=str_short),  dimension(:,:), allocatable :: output_compNames
  
  ! Silo database
  integer :: silo_comm,silo_rank,Nproc_node
  integer, dimension(:), allocatable :: group_ids,proc_nums
  real(WP), dimension(:,:),   allocatable ::  spatial_extents, myspatial_extents  !modify?
  real(WP), dimension(:,:),   allocatable :: Uspatial_extents,Umyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Vspatial_extents,Vmyspatial_extents
  real(WP), dimension(:,:),   allocatable :: Wspatial_extents,Wmyspatial_extents
  real(WP), dimension(:,:,:,:), allocatable :: data_extents,   mydata_extents     !modify?
  character(len=60), dimension(:), allocatable :: names
  integer, dimension(:), allocatable :: lnames,types
  
  ! Mesh 
  integer, parameter :: nGhostVisit=1
  integer :: nxout,nyout,nzout
  integer :: noverxmin_out,noverymin_out,noverzmin_out
  integer :: noverxmax_out,noverymax_out,noverzmax_out
  integer :: imin_out,jmin_out,kmin_out
  integer :: imax_out,jmax_out,kmax_out
  integer, dimension(3) :: lo_offset, hi_offset  !changed dimension from 2 to 3
  
  ! Buffers
  real(SP), dimension(:,:,:,:), allocatable :: out_buf  !I modified the dimension
  
contains
  
  ! ======================================= !
  !  Data to write to file                  !
  !   - Add data to write here              !
  !   - Include name of case in input file  !
  !      e.g. Visit outputs : Velocity      !
  ! ======================================= !
  subroutine visit_getData(n)
    implicit none
    integer, intent(in) :: n
    integer :: i,j,k,nn
    
    select case(adjustl(trim(output_names(n))))
       
    ! ============ Core Variables =====================

     ! Velocity field
    case ('Velocity') 
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'U', 'V', 'W' /)
       do k=kmino_,kmaxo_-1
          do j=jmino_,jmaxo_-1
             do i=imino_,imaxo_-1
                out_buf(i,j,k,1)=real((u(i,j,k,1)+u(i+1,j  ,k  ,1))*0.5_WP)
                out_buf(i,j,k,2)=real((v(i,j,k,1)+v(i  ,j+1,k  ,1))*0.5_WP)
                out_buf(i,j,k,3)=real((w(i,j,k,1)+w(i  ,j  ,k+1,1))*0.5_WP)
             end do
          end do
       end do
       
    case ('VelocityVar')
       output_types(n)='vector'
       output_compNames(:,n)=(/ 'varU', 'varV', 'varW' /)

       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          do k=kmino_,kmaxo_-1
             do j=jmino_,jmaxo_-1
                do i=imino_,imaxo_-1
                   out_buf(i,j,k,1)=out_buf(i,j,k,1)+real(((u(i,j,k,nn)+u(i+1,j  ,k  ,nn))*0.5_WP)**2*Var(nn),SP)
                   out_buf(i,j,k,2)=out_buf(i,j,k,2)+real(((v(i,j,k,nn)+v(i  ,j+1,k  ,nn))*0.5_WP)**2*Var(nn),SP)
                   out_buf(i,j,k,3)=out_buf(i,j,k,3)+real(((w(i,j,k,nn)+w(i  ,j  ,k+1,nn))*0.5_WP)**2*Var(nn),SP)
             end do
          end do
       end do
    end do

    case ('Viscosity') 
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(nu(:,:,:,1),SP)

    case ('ViscosityVar')
       output_types(n)='scalar'

       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof_nu
          do k=kmino_,kmaxo_-1
             do j=jmino_,jmaxo_-1
                do i=imino_,imaxo_-1
                   out_buf(i,j,k,1)=out_buf(i,j,k,1)+real(nu(i,j,k,nn)**2*Var(nn),SP)
                end do
             end do
          end do
       end do
          
    ! Level set
    case ('G')
       output_types(n)='scalar'
       out_buf(:,:,:,1)=real(G(:,:,:,1),SP)

    case ('GVar')
       output_types(n)='scalar'

       ! Second moment
       out_buf=0.0_WP
       do nn=2,ndof
          do k=kmino_,kmaxo_-1
             do j=jmino_,jmaxo_-1
                do i=imino_,imaxo_-1
                   out_buf(i,j,k,1)=out_buf(i,j,k,1)+real(G(i,j,k,nn)**2*Var(nn),SP)
                end do
             end do
          end do
       end do
          
    ! Unknown output
    case default
       call die('Unknown Visit output:'//trim(output_names(n))//'!')
    end select
    
    return
  end subroutine visit_getData
  
end module visit

! ======================================================================================================== !
! ======================================================================================================== !
!        You do not have to modify the code below this point to add a variable on a pre-defined mesh       !
! ======================================================================================================== !
! ======================================================================================================== !

! ======================================== !
!  Dump Visit Silo files - Initialization  !
! ======================================== !
subroutine visit_init
  use visit
  use io
  implicit none
  integer :: i,iunit,nchar
  logical :: file_exists
  character(len=5)  :: tmpchar
  character(len=60) :: tmpchar2
  real(WP),dimension(:),allocatable :: visit_times

  ! Read VisIt outputs from inputs
  call count_input('VisIt outputs',output_n)
  allocate(output_names(1:output_n))
  call read_input('VisIt outputs',output_names)
  ! Read output frequency form inputs
  call read_input('VisIt write freq',visit_freq)
  
  ! Check for arts.visit file and create folder if needed
  if (rank.eq.root) then
     inquire(file='Visit/multiUQ.visit',exist=file_exists)
     if (file_exists) then
        ! Get number of lines in file
        iunit = 32
        open(iunit,file="Visit/multiUQ.visit",form="formatted",iostat=ierr,status='old')  
        nout_time=0
        do
           read(iunit,*,end=1)
           nout_time=nout_time+1
        end do
1       continue
        close(iunit)        
        ! Read the file and keep times less than current time
        allocate(visit_times(nout_time))
        iunit = 32
        open(iunit,file="Visit/multiUQ.visit",form="formatted",iostat=ierr,status='old')  
        do i=1,nout_time
           ! Read file
           read(iunit,'(5A,60A)') tmpchar,tmpchar2
           ! Extract time from string
           nchar=len_trim(tmpchar2)
           read(tmpchar2(1:nchar-11),*) visit_times(i)
           ! Check if it is in the future and exit if true
           if (visit_times(i).ge.time-dt*1e-10_WP) then
              nout_time=i-1
              exit
           end if
        end do
        close(iunit)
        ! Write new file with only past times
        iunit = 32
        open(iunit,file="Visit/multiUQ.visit",form="formatted",iostat=ierr,status='replace')  
        do i=1,nout_time
           write(tmpchar2,'(ES12.5)') visit_times(i)
           tmpchar2='time_'//trim(adjustl(tmpchar2))//'/Visit.silo'
           write(iunit,'(A)') trim(adjustl(tmpchar2))
        end do
        deallocate(visit_times)
        close(iunit)
     else
        nout_time=0
        ! Create visit directory
        if (rank.eq.root) &
             call system('mkdir -p Visit')
     end if
  end if

  ! Set visit counter
  visit_count=1
  
  ! Number of procs per file
  Nproc_node=12
  
  ! Create group_ids 
  allocate(group_ids(nproc))
  do i=1,nproc
     group_ids(i)=ceiling(real(i,WP)/real(Nproc_node,WP))
  end do
  ! Create proc_nums
  allocate(proc_nums(nproc))
  do i=1,nproc
     proc_nums(i)=mod(i-1,Nproc_node)+1
  end do
  
  ! Split procs into new communicators
  call MPI_COMM_SPLIT(COMM,group_ids(rank+1),MPI_UNDEFINED,silo_comm,ierr)
  call MPI_COMM_RANK(silo_comm,silo_rank,ierr)
  silo_rank=silo_rank+1
  
  ! Parse variables to output
  !call parser_getsize('Visit outputs',output_n)
  !allocate(output_names(output_n))
  !call parser_readchararray('Visit outputs',output_names(1:output_n))
  
  ! Allocate arrays
  allocate(out_buf(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,3))
  output_max=3*output_n
  allocate(output_types(output_max))
  allocate(output_compNames(3,output_n))
  allocate(  spatial_extents(6,nproc)); allocate( myspatial_extents(6,nproc))
  allocate( Uspatial_extents(6,nproc)); allocate(Umyspatial_extents(6,nproc))
  allocate( Vspatial_extents(6,nproc)); allocate(Vmyspatial_extents(6,nproc))
  allocate( Wspatial_extents(6,nproc)); allocate(Wmyspatial_extents(6,nproc))
  allocate(  data_extents(2,nproc,3,output_max))
  allocate(mydata_extents(2,nproc,3,output_max))
  allocate(names (nproc))
  allocate(lnames(nproc))
  allocate(types (nproc))
  
  ! Create output limits because Visit doesn't support external ghost cells
  noverxmin_out=nGhostVisit; noverxmax_out=nGhostVisit; imin_out=imin_-nGhostVisit; imax_out=imax_+nGhostVisit
  noverymin_out=nGhostVisit; noverymax_out=nGhostVisit; jmin_out=jmin_-nGhostVisit; jmax_out=jmax_+nGhostVisit
  noverzmin_out=nGhostVisit; noverzmax_out=nGhostVisit; kmin_out=kmin_-nGhostVisit; kmax_out=kmax_+nGhostVisit
  if (rankx.eq.0   ) then; noverxmin_out=0; imin_out=imin_; end if
  if (ranky.eq.0   ) then; noverymin_out=0; jmin_out=jmin_; end if
  if (rankz.eq.0   ) then; noverzmin_out=0; kmin_out=kmin_; end if
  if (rankx.eq.px-1) then; noverxmax_out=0; imax_out=imax_; end if
  if (ranky.eq.py-1) then; noverymax_out=0; jmax_out=jmax_; end if
  if (rankz.eq.pz-1) then; noverzmax_out=0; kmax_out=kmax_; end if 
  nxout=imax_out-imin_out+1
  nyout=jmax_out-jmin_out+1
  nzout=kmax_out-kmin_out+1
  hi_offset(1)=noverxmax_out
  hi_offset(2)=noverymax_out
  hi_offset(3)=noverzmax_out
  lo_offset(1)=noverxmin_out
  lo_offset(2)=noverymin_out
  lo_offset(3)=noverzmin_out
  
  return 
end subroutine visit_init


! ===================================== !
!  Dump Visit Silo files - Write Files  !
! ===================================== !
subroutine visit_data
  use visit
  implicit none
  integer :: i,n,out
  integer :: dbfile,err,optlist,iunit
  character(len=str_medium) :: med_buffer
  character(len=5) :: dirname
  character(len=38) :: siloname
  character(len=22) :: folder,buffer
  logical :: file_exists
  
  ! Update output counter
  nout_time=nout_time+1

  ! Check if output this step
  visit_count=visit_count-1
  if (visit_count.gt.0) then
     return
  else 
     ! Reset counter
     visit_count=visit_freq
     ! Continue to write VisIt output
  end if

  ! Make foldername
  write(buffer,'(ES12.5)') time
  folder = 'Visit/time_'//trim(adjustl(buffer))

  if (rank.eq.root) then
     ! Create new directory
     call system('mkdir -p '// adjustl(trim(folder)))
     !call CREATE_FOLDER(trim(folder))
  end if
  call MPI_BARRIER(COMM,ierr)

  !  Create the group silo databases 
  ! =============================================

  ! Create the silo name for this processor
  write(siloname,'(A,I5.5,A)') folder//'/group',group_ids(rank+1),'.silo'

  ! Create the silo database
  if (silo_rank.eq.1) then
     err = dbcreate(siloname, len_trim(siloname), DB_CLOBBER, DB_LOCAL,"Silo database created with NGA", 30, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')
     ierr = dbclose(dbfile)
  end if
  
  !  Write the data
  ! =============================================
  do n=1,Nproc_node
     if (n.eq.silo_rank) then

        ! Open silo file 
        err = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)

        ! Create the silo directory
        write(dirname,'(I5.5)') proc_nums(rank+1)
        err = dbmkdir(dbfile,dirname,5,ierr)
        err = dbsetdir(dbfile,dirname,5)

        ! Write the variables
        mydata_extents=0.0_WP
        do out=1,output_n

           ! Get data to write
           call visit_getData(out)

           select case(trim(output_types(out)))

           case ('scalar') 
              ! Write the scalar
              med_buffer=output_names(out)
              err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                   "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1), &
                   (/nxout,nyout,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
              mydata_extents(1,rank+1,1,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))
              mydata_extents(2,rank+1,1,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,1))

           case ('vector') 
              ! Write the components
              do i=1,3
                 med_buffer=output_compNames(i,out)
                 err = dbputqv1(dbfile, trim(med_buffer),len_trim(med_buffer), &
                 "Mesh", 4, out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,i), &
                      (/nxout,nyout,nzout/), 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT,DB_F77NULL, ierr)
                 mydata_extents(1,rank+1,i,out)=minval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,i))
                 mydata_extents(2,rank+1,i,out)=maxval(out_buf(imin_out:imax_out,jmin_out:jmax_out,kmin_out:kmax_out,i))
              end do
              ! Write the expression
              med_buffer='{'//output_compNames(1,out)//','//output_compNames(2,out)//'}'
              err = dbputdefvars(dbfile,'defvars',7,1,output_names(out),len_trim(output_names(out)), &
                   DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)
                                           
           case default ! ---------------------------------------------------------------------------------------------
              call die('output_type not defined correctly for '//trim(output_names(out))//' in data write')
           end select
              
        end do

        !  Write the meshes
        ! =============================================
        ! Write the regular mesh
        err = dbmkoptlist(2, optlist)
        err = dbaddiopt(optlist, DBOPT_HI_OFFSET, hi_offset)
        err = dbaddiopt(optlist, DBOPT_LO_OFFSET, lo_offset)
        err = dbputqm(dbfile,"Mesh",4,'xc',2,'yc',2,"zc",2, &
        real(x(imin_out:imax_out+1),SP),real(y(jmin_out:jmax_out+1),SP),real(z(kmin_out:kmax_out+1),SP), &
             (/nxout+1,nyout+1,nzout+1/),3,DB_FLOAT,DB_COLLINEAR,optlist,ierr)
        err = dbfreeoptlist(optlist)

        ! Spatial extents (written in master silo's multimesh)
        myspatial_extents=0.0_WP
        myspatial_extents(1:3,rank+1)=(/x(imin_out  ),y(jmin_out  ),z(kmin_out  )/)
        myspatial_extents(4:6,rank+1)=(/x(imax_out+1),y(jmax_out+1),z(kmax_out+1)/)

        Umyspatial_extents=0.0_WP
        Umyspatial_extents(1:3,rank+1)=(/xm(imin_out-1),y(jmin_out  ),z(kmin_out  )/)
        Umyspatial_extents(4:6,rank+1)=(/xm(imax_out  ),y(jmax_out+1),z(kmax_out+1)/)

        Vmyspatial_extents=0.0_WP
        Vmyspatial_extents(1:3,rank+1)=(/x(imin_out  ),ym(jmin_out-1),z(kmin_out  )/)
        Vmyspatial_extents(4:6,rank+1)=(/x(imax_out+1),ym(jmax_out  ),z(kmax_out+1)/)

        Wmyspatial_extents=0.0_WP
        Wmyspatial_extents(1:3,rank+1)=(/x(imin_out  ),y(jmin_out  ),zm(kmin_out-1)/)
        Wmyspatial_extents(4:6,rank+1)=(/x(imax_out+1),y(jmax_out+1),zm(kmax_out  )/)

        ! Close silo file
        ierr = dbclose(dbfile)
     end if
     call MPI_BARRIER(silo_comm,ierr)
  end do
  
  ! Communicate extents
  call MPI_ALLREDUCE( myspatial_extents, spatial_extents,6*nproc,mpi_double_precision,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Umyspatial_extents,Uspatial_extents,6*nproc,mpi_double_precision,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Vmyspatial_extents,Vspatial_extents,6*nproc,mpi_double_precision,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(Wmyspatial_extents,Wspatial_extents,6*nproc,mpi_double_precision,MPI_SUM,COMM,ierr)
  call MPI_ALLREDUCE(mydata_extents,data_extents,2*nproc*3*output_max,mpi_double_precision,MPI_SUM,COMM,ierr)

  ! Create the master silo database (.visit file, multimesh, multivars)
  ! ===============================================================================
  if (rank.eq.root) then
     ! Append .visit file
     iunit = 32
     inquire(file="Visit/multiUQ.visit",exist=file_exists)
     if (file_exists) then
        open(iunit,file="Visit/multiUQ.visit",form="formatted",status="old",position="append",action="write")
     else
        open(iunit,file="Visit/multiUQ.visit",form="formatted",status="new",action="write")
     end if
     write(iunit,'(A)') folder(7:)//'/Visit.silo'
     close(iunit)
     
     ! Set length of names
     err = dbset2dstrlen(len(names))
     
     ! Create the master silo database
     err = dbcreate(folder//'/Visit.silo', len_trim(folder//'/Visit.silo'), DB_CLOBBER, DB_LOCAL, &
          "Silo database created with multiUQ", 34, DB_HDF5, dbfile)
     if(dbfile.eq.-1) call die('Could not create Silo file!')

     !  Write the multimeshes
     ! =======================
     do i=1,nproc
        write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/Mesh'
        lnames(i)=len_trim(names(i))
        types(i)=DB_QUAD_RECT
     end do
     err = dbmkoptlist(4,optlist)
     err = dbaddiopt(optlist, DBOPT_DTIME, time);
     err = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
     err = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
     err = dbputmmesh(dbfile,  "Mesh", 4, nproc, names,lnames, types, optlist, ierr)
     err = dbfreeoptlist(optlist)

     !  Write the multivars
     ! =======================
     do out=1,output_n
        select case(trim(output_types(out)))

        case ('scalar','scalar_Ucell','scalar_Vcell','scalar_Wcell')
           do i=1,nproc
              write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_names(out))
              lnames(i)=len_trim(names(i))
              types(i)=DB_QUADVAR
           end do
           err = dbmkoptlist(2, optlist)
           err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
           err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,1,out))
           err = dbputmvar(dbfile,output_names(out),len_trim(output_names(out)),nproc,names,lnames,types,optlist,ierr)
           err = dbfreeoptlist(optlist)

        case ('vector','vector_Ucell','vector_Vcell','vector_Wcell')
           do n=1,3
              do i=1,nproc
                 write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/'//trim(output_compNames(n,out))
                 lnames(i)=len_trim(names(i))
                 types(i)=DB_QUADVAR
              end do
              err = dbmkoptlist(2, optlist)
              err = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 2)
              err = dbadddopt(optlist, DBOPT_EXTENTS, data_extents(:,:,n,out))
              err = dbputmvar(dbfile,output_compNames(n,out),len_trim(output_compNames(n,out)), &
                   nproc,names,lnames,types,optlist,ierr)
              err = dbfreeoptlist(optlist)
           end do
           ! Write the expression
           med_buffer='{'//trim(output_compNames(1,out))// &
                ','//trim(output_compNames(2,out))//'}'
           err = dbputdefvars(dbfile,output_names(out),len_trim(output_names(out)),1, &
                output_names(out),len_trim(output_names(out)), &
                DB_VARTYPE_VECTOR,med_buffer,len_trim(med_buffer),DB_F77NULL,ierr)

        case default
           call die('output_type not defined correctly for '//trim(output_names(out))//'in multivar write')
        end select

     end do

     ! Close database
     ierr = dbclose(dbfile)
  end if
    
  return
end subroutine visit_data
  
