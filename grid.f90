! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!
!              Staggered Grid Layout
!
! y(j+1) - ==================================
!          |   cell (i,j)   |  cell (i+1,j) |
!          |                |               |
!          |                |               |
!  ym(j) - |-->    o P      |-->    o P     |
!          |  u             |  u            |
!          |       ^ v      |       ^ v     |
!          |       |        |       |       |
!   y(j) - ==================================
!          |       |        |       |       |
!         x(i)   xm(i)   x(i+1)  xm(i+1)  x(i+2)
!
! Assumptions:
!   - 2D
!   - uniform 

module grid
  use variables
  use parallel
  implicit none

  ! Global variables
  integer :: imin, imax, jmin, jmax, kmin, kmax
  ! Local variables
  integer :: imin_, imax_, jmin_, jmax_, kmin_, kmax_
  integer :: nx_, ny_, nz_
  ! Local variables with ghost cells
  integer :: imino_, imaxo_, jmino_, jmaxo_, kmino_, kmaxo_
  integer :: nxo_, nyo_, nzo_ 
  double precision, dimension(:), allocatable :: x,y,z
  double precision, dimension(:), allocatable :: xm,ym,zm
  double precision :: dx, dy, dz, dxi, dyi, dzi  

end module grid

!=========================!
!      Create Grid        !
!=========================!
subroutine grid_create
  use grid
  use io
  implicit none
  
  integer :: rem    
  integer :: i,j,k

  ! Read grid size
  call read_input('nx',nx)  
  call read_input('ny',ny)
  call read_input('nz',nz)

  ! Read the domain size
  call read_input('xmin',xmin)
  call read_input('xmax',xmax)
  call read_input('ymin',ymin)
  call read_input('ymax',ymax)
  call read_input('zmin',zmin)
  call read_input('zmax',zmax)

  ! Global index
  imin=1; imax=nx
  jmin=1; jmax=ny
  kmin=1; kmax=nz

  ! x local index 
  nx_=nx/px
  rem=mod(nx,px)
  if (rankx.lt.rem) nx_=nx_+1
  imin_=imin+rankx*(nx/px)+min(rankx,rem)
  imax_=imin_+nx_-1

  ! y local index 
  ny_=ny/py
  rem=mod(ny,py)
  if (ranky.lt.rem) ny_=ny_+1    
  jmin_=jmin+ranky*(ny/py)+min(ranky,rem)
  jmax_=jmin_+ny_-1

  ! z local index
  nz_=nz/pz
  rem=mod(nz,pz)
  if (rankz.lt.rem) nz_=nz_+1
  kmin_=kmin+rankz*(nz/pz)+min(rankz,rem)
  kmax_=kmin_+nz_-1

  ! Ghost variables
  imino_=imin_-nghost
  imaxo_=imax_+nghost
  jmino_=jmin_-nghost
  jmaxo_=jmax_+nghost
  kmino_=kmin_-nghost
  kmaxo_=kmax_+nghost
  nxo_=imaxo_-imino_+1
  nyo_=jmaxo_-jmino_+1
  nzo_=kmaxo_-kmino_+1

  ! X Y and Z values
  allocate(x(imino_:imaxo_+1))
  do i=imino_,imaxo_+1
     x(i)= & 
          xmin*(real(i)-real(imax+1))/(real(imin)-real(imax+1)) + &
          xmax*(real(imin)-real(i))/(real(imin)-real(imax+1))
  end do
  allocate(y(jmino_:jmaxo_+1))
  do j=jmino_,jmaxo_+1
     y(j)= & 
          ymin*(real(j)-real(jmax+1))/(real(jmin)-real(jmax+1)) + &
          ymax*(real(jmin)-real(j))/(real(jmin)-real(jmax+1))
  end do

  allocate(z(kmino_:kmaxo_+1))
  do k=kmino_,kmaxo_+1
     z(k) = &
            zmin*(real(k)-real(kmax+1))/(real(kmin)-real(kmax+1)) + &
            zmax*(real(kmin)-real(k))/(real(kmin)-real(kmax+1))
  end do

  ! Cell sizes
  dx=(xmax-xmin)/nx
  dy=(ymax-ymin)/ny
  dz=(zmax-zmin)/nz
  dxi=1.0/dx
  dyi=1.0/dy
  dzi=1.0/dz

  ! Xm Ym and Zm values
  allocate(xm(imino_:imaxo_))
  do i=imino_,imaxo_-1
     xm(i)=(x(i+1)+x(i))/2.0_WP
  end do
  xm(imaxo_)=xm(imaxo_-1)+dx

  allocate(ym(jmino_:jmaxo_))
  do j=jmino_,jmaxo_-1
     ym(j)=(y(j+1)+y(j))/2.0_WP
  end do
  ym(jmaxo_)=ym(jmaxo_-1)+dy

  allocate(zm(kmino_:kmaxo_))
  do k=kmino_,kmaxo_-1
     zm(k)=(z(k+1)+z(k))/2.0_WP !what is WP?
  end do
  zm(kmaxo_)=zm(kmaxo_-1)+dz

end subroutine grid_create


! module grid
!   use variables
!   implicit none

!   ! Global domain
!   integer :: imin , imax , jmin , jmax
!   ! With Ghost cells
!   integer :: imino, imaxo, jmino, jmaxo
!   integer :: nxo, nyo
!   double precision, dimension(:), allocatable :: x ,y
!   double precision, dimension(:), allocatable :: xm,ym
!   double precision :: dx, dy, dxi, dyi

! end module grid

! !=========================!
! !      Create Grid        !
! !=========================!
! subroutine grid_create
!   use grid
!   implicit none
  
!   integer :: i,j

!   ! Index
!   imin=1; imax=nx
!   jmin=1; jmax=ny
  

!   ! Ghost index
!   imino=imin-nghost
!   imaxo=imax+nghost
!   jmino=jmin-nghost
!   jmaxo=jmax+nghost
 
!   ! X and Y values
!   allocate(x(imino:imaxo))
!   do i=imino,imaxo
!      x(i)= & 
!           xmin*(real(i)-real(imax+1))/(real(imin)-real(imax+1)) + &
!           xmax*(real(imin)-real(i))/(real(imin)-real(imax+1))
!   end do
!   allocate(y(jmino:jmaxo))
!   do j=jmino,jmaxo
!      y(j)= & 
!           ymin*(real(j)-real(jmax+1))/(real(jmin)-real(jmax+1)) + &
!           ymax*(real(jmin)-real(j))/(real(jmin)-real(jmax+1))
!   end do

!   ! Xm and Ym valus
!   allocate(xm(imino:imaxo-1))
!   do i=imino,imaxo-1
!      xm(i)=(x(i+1)+x(i))/2.0_WP
!   end do
!   allocate(ym(jmino:jmaxo-1))
!   do j=jmino,jmaxo-1
!      ym(j)=(y(j+1)+y(j))/2.0_WP
!   end do

!   ! Cell sizes
!   dx=(xmax-xmin)/nx
!   dy=(ymax-ymin)/ny
!   dxi=1.0_WP/dx
!   dyi=1.0_WP/dy

!   ! Full domain
!   nxo=imaxo-imino+1
!   nyo=jmaxo-jmino+1

! end subroutine grid_create
