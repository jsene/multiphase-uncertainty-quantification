! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!=========================!
!    Global Variables     !
!=========================!
module variables
  implicit none

  ! Precision 
  integer, parameter  :: SP = kind(1.0)
  integer, parameter, private :: DP = kind(1.0d0)
  integer, parameter  :: WP = DP

  ! String lengths
  integer, parameter :: str_short  = 8
  integer, parameter :: str_medium = 64
  integer, parameter :: str_long   = 8192

  ! Simulation
  character(len=100) :: simu

  ! Number of cells
  integer :: nx, ny, nz

  ! Number of ghost cells on boundary
  integer, parameter :: nghost=2

  ! Physical size of domain
  double precision :: xmin, xmax, ymin, ymax, zmin, zmax

  ! Periodicity
  integer :: xper, yper, zper
  logical :: isxper, isyper, iszper

  ! Boundary conditions
  character(len=100) :: bc_y
  character(len=100) :: bc_z
  
  ! Iterations
  integer :: iter,niter

  ! Simulation complete?
  logical :: isdone

  ! Velocity field
  double precision, dimension(:,:,:,:), pointer :: u
  double precision, dimension(:,:,:,:), pointer :: v
  double precision, dimension(:,:,:,:), pointer :: w
  double precision :: vel_divergence

  ! Cell center velocity field (for output)
  double precision, dimension(:,:,:),   pointer :: um       
  double precision, dimension(:,:,:),   pointer :: vm       
  double precision, dimension(:,:,:),   pointer :: wm

  ! Right hand sides
  double precision, dimension(:,:,:,:), pointer :: rhsU     
  double precision, dimension(:,:,:,:), pointer :: rhsV
  double precision, dimension(:,:,:,:), pointer :: rhsW
  double precision, dimension(:,:,:,:), pointer :: rhsUold
  double precision, dimension(:,:,:,:), pointer :: rhsVold
  double precision, dimension(:,:,:,:), pointer :: rhsWold

  ! Pressure
  double precision, dimension(:,:,:,:), pointer :: p 

  ! Viscosity
  double precision, dimension(:,:,:,:), pointer :: nu      
  double precision                              :: nu_io
  
  ! Density
  double precision :: rho
  double precision :: rhoi

  ! Scalar
  double precision, dimension(:,:,:,:), pointer :: SC

  ! Level set
  double precision, dimension(:,:,:,:), pointer :: G
  double precision, dimension(:,:,:,:), pointer :: rhsG
  double precision, dimension(:,:,:,:), pointer :: rhsGold

  ! Time
  double precision :: dt
  double precision :: time
  double precision :: CFL

  ! Pressure convergence
  double precision :: cvg_criteria
  double precision, dimension(:,:,:), pointer :: r 
  integer :: pressure_steps, pressure_max_steps
  double precision :: p_res

  ! Mask (marks where walls are)
  double precision, dimension(:,:,:), pointer :: mask  

  ! Uncertainty quantification
  logical          :: is_uq     ! Run UQ
  integer          :: order     ! Order of basis
  integer          :: order_nu  ! Order of basis for nu
  integer          :: ndims     ! Number of uncertain variables
  double precision :: sigma     ! Nu uncertainty parameter
  double precision, dimension(:,:,:), allocatable :: var_u, var_v, var_w  

  ! Visit output
  integer :: visit_freq

end module variables

! ========================= !
! Initialize main variables !
! ========================= !
subroutine variables_init
  use variables
  use io
  implicit none
  
  ! Read main inputs
  call read_input('Simulation',simu)
  call read_input('CFL',CFL)
  call read_input('rho',rho)
  call read_input('nu',nu_io)
  call read_input('Pressure-max-steps',pressure_max_steps)
  call read_input('Pressure-cvg',cvg_criteria)
  call read_input('xper',isxper) 
  call read_input('yper',isyper)
  call read_input('zper',iszper)
  call read_input('niter',niter)
  call read_input('Uncertainty Quantification',is_uq)
  call read_input('Basis order',order,1)
  call read_input('Nu basis order',order_nu,1)
  call read_input('Uncertain Variables',ndims,1)

  return
end subroutine variables_init

