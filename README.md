multiUQ is an multiphase incompressible Navier-Stokes solver
with intrusive uncertertainty quantification
Author: Mark Owkes, PhD, Montana State University
        mark.owkes@montana.edu
        
        Edits to make code capable of 3-dimensional simulation
        produced by Jacob Senecal, Montana State University

Copyright (C) 2011 Mark Owkes

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>

============================================

Compile and run code:

  1) Adjust makefile with appropriate flags for your complier
  2) >> make  ! makes code
  3) mpirun -np 2 ./solver ! runs code

Contians: 

  *.f90		  : Fortran files
  makefile	  : complies code (uncomment correct flags)
  Examples       : Folder with example input files