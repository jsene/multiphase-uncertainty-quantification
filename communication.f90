! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module communication
  use parallel
  use variables
  use grid
  implicit none

contains
  !==========================!
  ! Communicate matrix along !
  ! edges of procs           !
  !==========================!
  subroutine comm_borders(A)
    use parallel
    use basis
    implicit none

    double precision, dimension(:,:,:,:), intent(inout)  :: A 
    integer            :: sizex, sizey, sizez
    integer            :: source, dest
    double precision, dimension(:,:,:,:), pointer :: bufferx_send, buffery_send, bufferz_send  
    double precision, dimension(:,:,:,:), pointer :: bufferx_recv, buffery_recv, bufferz_recv

    sizex=size(A,1)
    sizey=size(A,2)
    sizez=size(A,3)
   
    allocate(bufferx_send(nghost,sizey,sizez,ndof))
    allocate(buffery_send(sizex,nghost,sizez,ndof))
    allocate(bufferz_send(sizex,sizey,nghost,ndof))
    allocate(bufferx_recv(nghost,sizey,sizez,ndof))
    allocate(buffery_recv(sizex,nghost,sizez,ndof))
    allocate(bufferz_recv(sizex,sizey,nghost,ndof))

    ! ========== x direction =========== !

    ! Pass data to the left
    call mpi_cart_shift(comm_3D,0,-1,source,dest,ierr)
    tag=1      
    bufferx_send=A(1+nghost:nghost+nghost,:,:,:)
    bufferx_recv=A(nxo_-nghost+1:nxo_,:,:,:)
    call mpi_sendrecv(bufferx_send,sizez*sizey*nghost*ndof,mpi_double_precision,dest  ,tag, &
                      bufferx_recv,sizez*sizey*nghost*ndof,mpi_double_precision,source,tag,comm_3D,status,ierr)
    A(nxo_-nghost+1:nxo_,:,:,:)=bufferx_recv

    ! Pass data to the right
    tag=2
    bufferx_send=A(nxo_-nghost-nghost+1:nxo_-nghost,:,:,:)
    bufferx_recv=A(1:nghost,:,:,:)
    call mpi_sendrecv(bufferx_send,sizez*sizey*nghost*ndof,mpi_double_precision,source,tag, &
                      bufferx_recv,sizez*sizey*nghost*ndof,mpi_double_precision,dest  ,tag,comm_3D,status,ierr)
    A(1:nghost,:,:,:)=bufferx_recv
    
    ! ========== y direction =========== !
    
      ! Pass data to the left
    call mpi_cart_shift(comm_3D,1,-1,source,dest,ierr)
    tag=3
    buffery_send=A(:,1+nghost:nghost+nghost,:,:)
    buffery_recv=A(:,nyo_-nghost+1:nyo_,:,:)
    call mpi_sendrecv(buffery_send,sizez*sizex*nghost*ndof,mpi_double_precision,dest  ,tag, &
                      buffery_recv,sizez*sizex*nghost*ndof,mpi_double_precision,source,tag,comm_3D,status,ierr)
    A(:,nyo_-nghost+1:nyo_,:,:)=buffery_recv

    ! Pass data to the right
    tag=4
    buffery_send=A(:,nyo_-nghost-nghost+1:nyo_-nghost,:,:)
    buffery_recv=A(:,1:nghost,:,:)
    call mpi_sendrecv(buffery_send,sizez*sizex*nghost*ndof,mpi_double_precision,source,tag, &
                      buffery_recv,sizez*sizex*nghost*ndof,mpi_double_precision,dest  ,tag,comm_3D,status,ierr)
    A(:,1:nghost,:,:)=buffery_recv

    ! =========== z direction ===========!

    ! Pass data to the left
    call mpi_cart_shift(comm_3D,2,-1,source,dest,ierr)
    tag=5
    bufferz_send=A(:,:,1+nghost:nghost+nghost,:)
    bufferz_recv=A(:,:,nzo_-nghost+1:nzo_,:)
    call mpi_sendrecv(bufferz_send,sizey*sizex*nghost*ndof,mpi_double_precision,dest  ,tag, &
                      bufferz_recv,sizey*sizex*nghost*ndof,mpi_double_precision,source,tag,comm_3D,status,ierr)
    A(:,:,nzo_-nghost+1:nzo_,:)=bufferz_recv

    ! Pass data to the right
    tag=6
    bufferz_send=A(:,:,nzo_-nghost-nghost+1:nzo_-nghost,:)
    bufferz_recv=A(:,:,1:nghost,:)
    call mpi_sendrecv(bufferz_send,sizey*sizex*nghost*ndof,mpi_double_precision,source,tag, &
                      bufferz_recv,sizey*sizex*nghost*ndof,mpi_double_precision,dest  ,tag,comm_3D,status,ierr)
    A(:,:,1:nghost,:)=bufferz_recv

    deallocate(bufferx_send)
    deallocate(buffery_send)
    deallocate(bufferz_send)
    deallocate(bufferx_recv)
    deallocate(buffery_recv)
    deallocate(bufferz_recv)
     
  end subroutine comm_borders

  !==========================!
  ! Communicate matrix along !
  ! edges of procs           !
  !==========================!
  subroutine comm_borders_2d(A)
    use parallel
    use basis
    implicit none

    double precision, dimension(:,:,:), intent(inout)  :: A
    integer            :: sizex, sizey, sizez
    integer            :: source, dest
    double precision, dimension(:,:,:), pointer :: bufferx_send, buffery_send, bufferz_send
    double precision, dimension(:,:,:), pointer :: bufferx_recv, buffery_recv, bufferz_recv

    sizex=size(A,1)
    sizey=size(A,2)
    sizez=size(A,3)

    allocate(bufferx_send(nghost,sizey,sizez))
    allocate(buffery_send(sizex,nghost,sizez))
    allocate(bufferz_send(sizex,sizey,nghost))
    allocate(bufferx_recv(nghost,sizey,sizez))
    allocate(buffery_recv(sizex,nghost,sizez))
    allocate(bufferz_recv(sizex,sizey,nghost))

    ! ========== x direction =========== !

    ! Pass data to the left
    call mpi_cart_shift(comm_3D,0,-1,source,dest,ierr)
    tag=1      
    bufferx_send=A(1+nghost:nghost+nghost,:,:)
    bufferx_recv=A(nxo_-nghost+1:nxo_,:,:)
    call mpi_sendrecv(bufferx_send,sizez*sizey*nghost,mpi_double_precision,dest  ,tag, &
                      bufferx_recv,sizez*sizey*nghost,mpi_double_precision,source,tag,comm_3D,status,ierr)
    A(nxo_-nghost+1:nxo_,:,:)=bufferx_recv

    ! Pass data to the right
    tag=2
    bufferx_send=A(nxo_-nghost-nghost+1:nxo_-nghost,:,:)
    bufferx_recv=A(1:nghost,:,:)
    call mpi_sendrecv(bufferx_send,sizez*sizey*nghost,mpi_double_precision,source,tag, &
                      bufferx_recv,sizez*sizey*nghost,mpi_double_precision,dest  ,tag,comm_3D,status,ierr)
    A(1:nghost,:,:)=bufferx_recv
    
    ! ========== y direction =========== !
    
      ! Pass data to the left
    call mpi_cart_shift(comm_3D,1,-1,source,dest,ierr)
    tag=3
    buffery_send=A(:,1+nghost:nghost+nghost,:)
    buffery_recv=A(:,nyo_-nghost+1:nyo_,:)
    call mpi_sendrecv(buffery_send,sizez*sizex*nghost,mpi_double_precision,dest  ,tag, &
                      buffery_recv,sizez*sizex*nghost,mpi_double_precision,source,tag,comm_3D,status,ierr)
    A(:,nyo_-nghost+1:nyo_,:)=buffery_recv

    ! Pass data to the right
    tag=4
    buffery_send=A(:,nyo_-nghost-nghost+1:nyo_-nghost,:)
    buffery_recv=A(:,1:nghost,:)
    call mpi_sendrecv(buffery_send,sizez*sizex*nghost,mpi_double_precision,source,tag, &
                      buffery_recv,sizez*sizex*nghost,mpi_double_precision,dest  ,tag,comm_3D,status,ierr)
    A(:,1:nghost,:)=buffery_recv

    !========== z direction =========== !

    ! Pass data to the left
    call mpi_cart_shift(comm_3D,2,-1,source,dest,ierr)
    tag=5
    bufferz_send=A(:,:,1+nghost:nghost+nghost)
    bufferz_recv=A(:,:,nzo_-nghost+1:nzo_)
    call mpi_sendrecv(bufferz_send,sizey*sizex*nghost,mpi_double_precision,dest  ,tag, &
                      bufferz_recv,sizey*sizex*nghost,mpi_double_precision,source,tag,comm_3D,status,ierr)
    A(:,:,nzo_-nghost+1:nzo_)=bufferz_recv

    ! Pass data to the right
    tag=6
    bufferz_send=A(:,:,nzo_-nghost-nghost+1:nzo_-nghost)
    bufferz_recv=A(:,:,1:nghost)
    call mpi_sendrecv(bufferz_send,sizey*sizex*nghost,mpi_double_precision,source,tag, &
                      bufferz_recv,sizey*sizex*nghost,mpi_double_precision,dest  ,tag,comm_3D,status,ierr)
    A(:,:,1:nghost)=bufferz_recv

    deallocate(bufferx_send)
    deallocate(buffery_send)
    deallocate(bufferz_send)
    deallocate(bufferx_recv)
    deallocate(buffery_recv)
    deallocate(bufferz_recv)

     return
  end subroutine comm_borders_2d


  !=========================!
  !     Sum real number     !
  !=========================!
  subroutine sum_real(A,B)
    implicit none
    double precision, intent(in)  :: A
    double precision, intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_double_precision,mpi_sum,comm_3D,ierr)

    return
  end subroutine sum_real

  !=========================!
  !    Max of real number   !
  !=========================!
  subroutine max_real(A,B)
    implicit none
    double precision, intent(in)  :: A
    double precision, intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_double_precision,mpi_max,comm_3D,ierr)

    return
  end subroutine max_real

  !=========================!
  !    Min of real number   !
  !=========================!
  subroutine min_real(A,B)
    implicit none
    double precision, intent(in)  :: A
    double precision, intent(out) :: B

    call mpi_allreduce(A,B,1,mpi_double_precision,mpi_min,comm_3D,ierr)

    return
  end subroutine min_real

end module communication
