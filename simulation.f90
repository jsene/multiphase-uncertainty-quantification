! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module simulation
  use variables
  use grid
  implicit none

  ! Dummy module

end module simulation

!===========================!
!      Initialization       !
!===========================!
subroutine simulation_init
  use simulation
  use math
  use io
  implicit none

  integer :: i,j,k
  double precision, parameter :: U_o=0.05_WP
  double precision, parameter :: U_p=0.005_WP

  ! Initialize mask
  ! mask=1. : Normal cell
  ! mask=0. : Wall
  allocate(mask(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  mask=1.0_WP

  ! Initialize velocity field
  select case(trim(simu))

  case ('poiseuile')
     u=0.0_WP
     u(:,:,:,1)=1.0_WP
     if (ranky.eq.0)    u(:,jmin-1,:,1)=-1.0_WP
     if (ranky.eq.py-1) u(:,jmax+1,:,1)=-1.0_WP
     if (rankz.eq.0)    u(:,:,kmin-1,1)=-1.0_WP
     if (rankz.eq.pz-1) u(:,:,kmax+1,1)=-1.0_WP
     v=0.0_WP
     w=0.0_WP

  case ('mixing-layer')
     do j=jmin_,jmax_
        do i=imin_,imax_
           ! U velocity
           u(i,j,:,:)=U_o*tanh(20*ym(j)/(ymax-ymin))+U_p*sin(pi/2*(xmax-xmin)*x(i))
        end do
     end do
    u(:,jmax+1,:,:)=+U_o
    u(:,jmin-1,:,:)=-U_o
    u(imax+1,:,:,:)=u(imin,:,:,:)
    u(imin-1,:,:,:)=u(imax,:,:,:)

     ! V velocity
     v=0.0_WP

  case ('cylinder')
     u=0.0_WP
     v=0.0_WP
     u(:,:,:,1)=3.0d-2
     do j=jmino_,jmaxo_
        u(:,j,:,1)=u(:,j,:,1)+cos(2*pi*y(j))*u(:,j,:,1)*0.1_WP
        do i=imino_,imaxo_
           if (sqrt(x(i)**2+ym(j)**2).lt.0.5_WP) then
              u(i,j,:,1)=0.0_WP
              mask(i,j,:)=0.0_WP
           end if
        end do
     end do
     if (rankx.eq.0) u(imin,:,:,1)=3.0d-2

  case ('square')
     u=0.0_WP
     v=0.0_WP
     w=0.0_WP
     u(:,:,:,1)=3.0d-2
     do j=jmino_,jmaxo_
        do i=imino_,imaxo_
           if (abs(x(i)).le.0.25_WP.and.abs(ym(j)).le.0.5_WP) then
              u(i,j,:,1)=0.0_WP
              mask(i,j,:)=0.0_WP
           end if
        end do
     end do
     
  case ('uncertainvelocity')
     u=0.0_WP
     u(:,:,:,1)=1.0_WP
     u(:,:,:,2)=0.1_WP
     v=0.0_WP

  case default
     call die('Unknown simulation: '//trim(simu))
  end select

  ! Initialize level set
  call levelset_init

  ! Form masks on edges of domain
  if (.not.isxper) then
     if (rankx.eq.0)    mask(imin-1,:,:)=0.0_WP
     if (rankx.eq.px-1) mask(imax+1,:,:)=0.0_WP
  end if
  if (.not.isyper) then
     if (ranky.eq.0)    mask(:,jmin-1,:)=0.0_WP
     if (ranky.eq.py-1) mask(:,jmax+1,:)=0.0_WP
  end if
  if (.not.iszper) then
     if (rankz.eq.0)    mask(:,:,kmin-1)=0.0_WP
     if (rankz.eq.pz-1) mask(:,:,kmax+1)=0.0_WP
  end if
  
  return
end subroutine simulation_init

!===========================!
!     Solution Update       !
!===========================!
subroutine simulation_step
  use simulation
  implicit none

  ! Transport interface
  call levelset_step

  ! Calculate predictor step velocity
  call timing_pause(3)
  call timing_resume(1)
  call velocity_calc
  call timing_pause(1)
  call timing_resume(3)

  ! Apply boundary conditions
  call velocity_bc
  ! Calculate correction step pressure
  call timing_pause(3)
  call timing_resume(2)
  call pressure_calc
  call timing_pause(2)
  call timing_resume(3)

  ! Correct velocity
  call velocity_correct

  ! Compute divergence
  call velocity_divergence

  return
end subroutine simulation_step
