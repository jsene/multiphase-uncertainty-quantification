! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module basis
  use variables
  use polynomial
  implicit none

  integer                                     :: ndof      ! number of degrees of freedom 
  integer                                     :: ndof_nu   ! number of dof for nu
  integer,    dimension(:,:),         pointer :: basisComb ! multidim  basis combination
  type(poly), dimension(:),           pointer :: phi       ! Basis functions (1D)
  type(poly), dimension(:),           pointer :: dphi      ! Derivative
  type(poly), dimension(:),           pointer :: ddphi     ! 2nd Derivative
  double precision, dimension(:,:,:), pointer :: M         ! Mass matrix
  double precision, dimension(:),     pointer :: Var       ! Variance of each basis
  
end module basis


!===========================!
! Initialize legendre basis !
!===========================!
subroutine basis_init
  use basis
  use polynomial
  use io
  implicit none

  integer :: i

  ! ndof=product{order+k,k=1..dim}/dim!
  ndof=1
  do i=1,ndims
     ndof=ndof*(order+i)
  end do
  ndof=ndof/factorial_nm(ndims,1)

  ndof_nu=1
  do i=1,ndims
     ndof_nu=ndof_nu*(order_nu+i)
  end do
  ndof_nu=ndof_nu/factorial_nm(ndims,1)

  ! Generate ndims basis combinations info
  call basis_combinations

  ! Generate scaled Legendre polynomials
  call basis_legendre 

  ! Calculate mass matrix
  call basis_matrix

  return
end subroutine basis_init


!=================================!
! Generate basis combination info !
! basis(dof,
!=================================!
subroutine basis_combinations
  use basis
  implicit none

  integer :: or,n, count

  ! Allocate basis
  allocate(basisComb(max(ndof,ndof_nu),ndims))
  basisComb=0

  count=1
  do or=0,max(order,order_nu)
     call basis_combinations_calc(count,or,1)
  end do

  ! print *, 'ndof=',ndof
  ! do n=1,ndof_nu
  !    write(*,'(10I3)') basisComb(n,:)
  ! end do

end subroutine basis_combinations

recursive subroutine basis_combinations_calc(count,or,dim)
  use basis
  implicit none

  ! Inputs
  integer :: count
  integer, intent(in) :: or, dim

  ! Variables
  integer :: n

  if (dim.gt.ndims) return

  do n=max(or-sum(basisComb(count,1:dim-1)),0),0,-1
     basisComb(count:max(ndof,ndof_nu),dim)=n
     if (dim.eq.ndims) then
        count=count+1
        return
     end if
     call basis_combinations_calc(count,or,dim+1)
  end do

  return
end subroutine basis_combinations_calc


!======================================!
! Generate scaled legendre polynomials !
!======================================!
subroutine basis_legendre
  use basis
  use polynomial
  implicit none

  integer :: i,k
  type(poly) :: polytmp

  ! Allocate basis
  allocate(  phi(0:max(order,order_nu)+1))
  allocate( dphi(0:max(order,order_nu)+1))
  allocate(ddphi(0:max(order,order_nu)+1))

  do i=0,max(order,order_nu)+1
     nullify(  phi(i)%a);nullify(  phi(i)%p)
     nullify( dphi(i)%a);nullify( dphi(i)%p)
     nullify(ddphi(i)%a);nullify(ddphi(i)%p)
  end do
  
  ! Form phi polynomials: phi_i(x)=i!/(2i)! d^i/dx^i{(x^2-1)^i}
  do i=0,max(order,order_nu)+1

     ! Allocate temporary polynomial for (x^2-1)^i
     polytmp%n=i+1
     allocate(polytmp%a(polytmp%n))
     allocate(polytmp%p(polytmp%n))

     ! Compute (x^2-1)^i using binomial theorem
     ! (x+y)^i = sum{C(i,k)*x^(i-k)*y^k,k=0..i}
     ! where C(i,k) is the binomial coefficient
     do k=0,i
        polytmp%a(k+1)=real(binomial(i,k)*(-1)**k) 
        polytmp%p(k+1)=2*(i-k)
     end do
     
     ! Take ith derivative of (x^2-1)^i
     call derivative_n(polytmp,phi(i),i) 

     ! Multiply by i!/(2i)! to form phi
     do k=1,phi(i)%n
        phi(i)%a(k)=phi(i)%a(k)/real(factorial_nm(2*i,i+1))
     end do

     ! Compute derivative of phi in dphi
     call derivative_n(phi(i),dphi(i),1)

     ! Compute derivative of dphi in ddphi
     call derivative_n(dphi(i),ddphi(i),1)


     ! Deallocate temporary polynomial
     polytmp%n=0
     deallocate(polytmp%a)
     deallocate(polytmp%p)

     ! print *,'===========',i,'th Basis function ==========='
     ! do k=1,phi(i)%n
     !    print *,phi(i)%a(k),'x^',phi(i)%p(k)
     ! end do

  end do

  return
end subroutine basis_legendre

!=======================================!
! Generate mass matrix                  !
! M=< phi_i phi_j phi_k > / < phi_k^2 > !      
!=======================================!
subroutine basis_matrix
  use basis
  implicit none
  
  integer :: count, dir
  integer ::  i, j, k
  integer :: ii,jj,kk
  type(poly) :: polytmp
  double precision, parameter :: x_min=-1.0_WP
  double precision, parameter :: x_max=+1.0_WP

  ! Allocate matrix
  allocate(M(max(ndof,ndof_nu),ndof,ndof))
  allocate(Var(ndof))
  M=1.0_WP
  Var=1.0_WP
  
  do k=1,ndof

     ! Calculate denominator =============================================== !
     Var(k)=1.0_WP

     ! Compute integral of phi_k * phi_k over [xmin,xmax]^3
     do dir=1,ndims

        ! Allocate polytmp
        polytmp%n=phi(basisComb(k,dir))%n*phi(basisComb(k,dir))%n
        allocate(polytmp%a(polytmp%n))
        allocate(polytmp%p(polytmp%n))

        ! Form phi_i*phi_j in direction dir in polytmp
        count=0
        do kk=1,phi(basisComb(k,dir))%n
           count=count+1
           polytmp%a(count)=phi(basisComb(k,dir))%a(kk)*phi(basisComb(k,dir))%a(kk)
           polytmp%p(count)=phi(basisComb(k,dir))%p(kk)+phi(basisComb(k,dir))%p(kk)
        end do

        ! Compute integral
        Var(k)=Var(k)*integral(polytmp,x_min,x_max)

        ! Deallocate temporary polynomial
        polytmp%n=0
        deallocate(polytmp%a)
           deallocate(polytmp%p)

        end do

        ! Calculate numerator and form matrix =============================================== !       
        do j=1,ndof
           do i=1,max(ndof,ndof_nu)

              ! Compute integral of phi_i phi_j * phi_k over [xmin,xmax]^3
              do dir=1,ndims

                 ! Allocate polytmp
                 polytmp%n=phi(basisComb(i,dir))%n*phi(basisComb(j,dir))%n*phi(basisComb(k,dir))%n
                 allocate(polytmp%a(polytmp%n))
                 allocate(polytmp%p(polytmp%n))

                 ! Form phi_i*phi_j in direction dir in polytmp
                 count=0
                 do kk=1,phi(basisComb(k,dir))%n
                    do jj=1,phi(basisComb(j,dir))%n
                       do ii=1,phi(basisComb(i,dir))%n
                          count=count+1
                          polytmp%a(count)=phi(basisComb(i,dir))%a(ii)*phi(basisComb(j,dir))%a(jj)*phi(basisComb(k,dir))%a(kk)
                          polytmp%p(count)=phi(basisComb(i,dir))%p(ii)+phi(basisComb(j,dir))%p(jj)+phi(basisComb(k,dir))%p(kk)
                       end do
                    end do
                 end do

                 ! Compute integral
                 M(i,j,k)=M(i,j,k)*integral(polytmp,x_min,x_max)

                 ! Deallocate temporary polynomial
                 polytmp%n=0
                 deallocate(polytmp%a)
                 deallocate(polytmp%p)

              end do

           end do
        end do

        M(:,:,k)=M(:,:,k)/Var(k)
        
     end do

     ! Scale Variance vector ??? Need to look into this (if uncommented, revisit level set projections)
     ! Var=Var/2.0_WP

  return
end subroutine basis_matrix
