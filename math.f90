! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module math
  use grid
  implicit none

  double precision, parameter :: Pi = &
       3.14159265358979323846264338327950288419716939937510

  ! Guass Quadrature
  integer :: gauss_n
  double precision, dimension(:), allocatable :: gauss_w,gauss_x

contains

  !=========================================!
  ! Operator AtV : computes vec(A)^t*vec(B) !
  ! where A and B are matrices, and vec(A)  !
  ! and vec(B) are their vector forms       !
  !=========================================!
  function AtB(A,B)
    use communication
    implicit none

    ! Inputs
    double precision, dimension(nxo_,nyo_,nzo_), intent(in) :: A
    double precision, dimension(nxo_,nyo_,nzo_), intent(in) :: B

    ! Variables
    integer :: i,j,k
    double precision :: AtB_local

    ! Outputs
    double precision :: AtB

    ! Zero Solution
    AtB_local=0.0_WP

    ! Compute A^t*B on interior of domain
    do k=1+nghost,nzo_-nghost
       do j=1+nghost,nyo_-nghost
          do i=1+nghost,nxo_-nghost
             AtB_local=AtB_local+A(i,j,k)*B(i,j,k)
          end do
       end do
    end do

    ! Communicate
    call sum_real(AtB_local,AtB)

    return
  end function AtB

  !===================================!
  ! Operator ApBC(f) : computes A+B*C !
  ! for matrices A & C and constant B !
  !===================================!
  function ApBC(A,B,C)
    use grid
    implicit none

    ! Inputs
    double precision, dimension(nxo_,nyo_,nzo_), intent(in) :: A
    double precision,                            intent(in) :: B
    double precision, dimension(nxo_,nyo_,nzo_), intent(in) :: C

    ! Variables
    integer :: i,j,k

    ! Outputs
    double precision, dimension(nxo_,nyo_,nzo_) :: ApBC

    ! Zero ApBC
    ApBC=0.0_WP

    ! Calculate laplacian of A on interior of domain
    do k=1,nzo_
       do j=1,nyo_
          do i=1,nxo_
             ApBC(i,j,k)=A(i,j,k)+B*C(i,j,k)
          end do
       end do
    end do

    return
  end function ApBC

end module math

!===================================!
!    Initialize Gauss Quadrature    !
!===================================!
subroutine gauss_init
  use math
  implicit none

  ! Number of quadrature points
  gauss_n=4

  ! Allocate weights and locations
  allocate(gauss_w(gauss_n))
  allocate(gauss_x(gauss_n))

  ! Compute weights and locations
  gauss_w = (/ 0.652145154862526, 0.652145154862526, 0.347854845137454, 0.347854845137454 /) 
  gauss_x = (/ 0.339981043583856,-0.339981043583856, 0.861136311590453,-0.861136311590453 /)

  print *,'Warning: Gauss quadrature only implemented with 4 points'

  return
end subroutine gauss_init
