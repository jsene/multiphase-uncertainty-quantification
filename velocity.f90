! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module velocity
  use grid
  use variables
  use basis
  use communication
  implicit none

  ! Source term
  double precision, dimension(:), allocatable :: sourceU
  double precision :: mdot, mdot_init, mdot_local

end module velocity

!==============================!
!        Initialization        !
!==============================!
subroutine velocity_init
  use velocity
  use io
  implicit none

  ! Allocate arrays
  allocate(u      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(v      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(w      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(um     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(vm     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(wm     (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(rhsU   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(rhsV   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(rhsW   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(rhsUold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(rhsVold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(rhsWold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(sourceU(1:ndof))

  ! Read boundary conditions
  call read_input('BC-y',bc_y)
  call read_input('BC-z',bc_z)

  return
end subroutine velocity_init

!==============================!
! Calculate Predictor Velocity !
!==============================!
subroutine velocity_calc
  use velocity
  use io
  implicit none
  
  integer :: i,j,k,t
  double precision :: dt_local

  ! Calculate rhs
  call velocity_rhs

  ! Calculate source
  call velocity_source

  ! Calculate timestep
  dt_local=CFL*dx/max(maxval(abs(u)),maxval(abs(v)),maxval(abs(w)))                           
  call min_real(dt_local,dt)

  if (iter.eq.1) then
     ! Update velocity using explict Euler on first step
     do t=1,ndof
        do j=jmin_,jmax_
           do i=imin_,imax_
		      do k=kmin_,kmax_
                 u(i,j,k,t)=u(i,j,k,t)+dt*(rhsU(i,j,k,t)+sourceU(t))*mask(i,j,k)*mask(i-1,j,k)
                 v(i,j,k,t)=v(i,j,k,t)+dt*(rhsV(i,j,k,t)           )*mask(i,j,k)*mask(i,j-1,k)
		         w(i,j,k,t)=w(i,j,k,t)+dt*(rhsW(i,j,k,t)           )*mask(i,j,k)*mask(i,j,k-1)
                 ! save rhs
                 rhsUold(i,j,k,t)=rhsU(i,j,k,t)+sourceU(t)
                 rhsVold(i,j,k,t)=rhsV(i,j,k,t)
		         rhsWold(i,j,k,t)=rhsW(i,j,k,t)
		      end do
           end do
        end do
     end do
  else
     ! Update using 2 step Adams-Bashforth
     do t=1,ndof
        do j=jmin_,jmax_
           do i=imin_,imax_
		      do k=kmin_,kmax_
                 u(i,j,k,t)=u(i,j,k,t)+dt*(1.5_WP*(rhsU(i,j,k,t)+sourceU(t)) &
                           -0.5_WP*rhsUold(i,j,k,t))*mask(i,j,k)*mask(i-1,j,k)
                 v(i,j,k,t)=v(i,j,k,t)+dt*(1.5_WP*rhsV(i,j,k,t) &
                           -0.5_WP*rhsVold(i,j,k,t))*mask(i,j,k)*mask(i,j-1,k)
		         w(i,j,k,t)=w(i,j,k,t)+dt*(1.5_WP*rhsW(i,j,k,t) &
                           -0.5_WP*rhsWold(i,j,k,t))*mask(i,j,k)*mask(i,j,k-1)
		! Save old rhs
               rhsUold(i,j,k,t)=rhsU(i,j,k,t)+sourceU(t)                   
               rhsVold(i,j,k,t)=rhsV(i,j,k,t)
		       rhsWold(i,j,k,t)=rhsW(i,j,k,t)
		      end do
           end do
        end do
     end do
  end if

  call comm_borders(u)
  call comm_borders(v)
  call comm_borders(w)
  
  return
end subroutine velocity_calc

!==============================!
! Calculate Corrected Velocity !
!==============================!
subroutine velocity_correct
  use velocity
  implicit none
  integer :: i,j,k,d             
  
  do d=1,ndof
     do j=jmin_,jmax_
        do i=imin_,imax_
	       do k=kmin_,kmax_
              u(i,j,k,d)=u(i,j,k,d)-dt*rhoi*(p(i,j,k,d)-p(i-1,j,k,d))*dxi*mask(i,j,k)*mask(i-1,j,k)
              v(i,j,k,d)=v(i,j,k,d)-dt*rhoi*(p(i,j,k,d)-p(i,j-1,k,d))*dyi*mask(i,j,k)*mask(i,j-1,k)
              w(i,j,k,d)=w(i,j,k,d)-dt*rhoi*(p(i,j,k,d)-p(i,j,k-1,d))*dzi*mask(i,j,k)*mask(i,j,k-1)
	       end do
        end do
     end do
  end do

  call comm_borders(u)
  call comm_borders(v)
  call comm_borders(w)

  return
end subroutine velocity_correct

!==============================!
! Velocity boundary conditions !       
!==============================!
subroutine velocity_bc
  use velocity
  implicit none

  ! Left right faces ------------
  if (isxper) then  ! apply periodic condition
     if(rankx.eq.px-1) then             
        u(imax+1,:,:,:)=u(imax,:,:,:)
        v(imax+1,:,:,:)=v(imax,:,:,:)
        w(imax+1,:,:,:)=w(imax,:,:,:)
     end if
  end if


  ! Top bottom faces ------------
  if (isyper) then  ! apply periodic condition
     if(ranky.eq.py-1) then
        u(:,jmax+1,:,:)=u(:,jmax,:,:)
        v(:,jmax+1,:,:)=v(:,jmax,:,:)
        w(:,jmax+1,:,:)=w(:,jmax,:,:)
     end if
  else ! apply boundary condition on y-walls
     select case(bc_y)
     case ('no-slip')
        if (ranky.eq.0) then
           u(:,jmin-1,:,1)=-u(:,jmin,:,1)
           v(:,jmin  ,:,1)=0.0_WP
           w(:,jmin-1,:,1)=-w(:,jmin,:,1)
        end if
        if (ranky.eq.py-1) then
           u(:,jmax+1,:,1)=-u(:,jmax,:,1)
           v(:,jmax  ,:,1)=0.0_WP
           w(:,jmax+1,:,1)=-w(:,jmax,:,1)
        end if
     case ('Dirichlet')
        ! Do nothing
     case default
        call die('Unknown boundary condition for y: '//bc_y)
     end select
  end if

  ! Front back faces ————————————                
  if (iszper) then  ! apply periodic condition
     if(rankz.eq.pz-1) then
        u(:,:,kmax+1,:)=u(:,:,kmax,:)
        v(:,:,kmax+1,:)=v(:,:,kmax,:)
        w(:,:,kmax+1,:)=w(:,:,kmax,:)
     end if
  else ! apply boundary condition on z-walls
    select case(bc_z)
     case ('no-slip')
        if (rankz.eq.0) then
           u(:,:,kmin-1,1)=-u(:,:,kmin,1)
           v(:,:,kmin  ,1)=0.0_WP
           w(:,:,kmin-1,1)=-w(:,:,kmin,1)
        end if
        if (ranky.eq.py-1) then
           u(:,:,kmax+1,1)=-u(:,:,kmax,1)
           v(:,:,kmax,  1)=0.0_WP
           w(:,:,kmax+1,1)=-w(:,:,kmax,1)
        end if
     case ('Dirichlet')
        ! Do nothing
     case default
        call die('Unknown boundary condition for z: '//bc_z)
     end select
  end if

  return
end subroutine velocity_bc

!==============================!
!         Velocity RHS         !     
!==============================!
subroutine velocity_rhs
  use velocity
  use io
  implicit none

  integer :: i,j,k,t,c,l                                 
  double precision :: u_here, v_here, w_here
  double precision :: term1, term2, term3, term4, term5
  double precision :: nu_pos_yx, nu_neg_yx, nu_pos_zx, nu_neg_zx
  double precision :: nu_pos_xy, nu_neg_xy, nu_pos_zy
  double precision :: nu_neg_zy, nu_pos_xz, nu_neg_xz
  double precision :: nu_neg_yz, nu_pos_yz

  ! Zero rhs
  rhsU=0.0_WP
  rhsV=0.0_WP
  rhsW=0.0_WP

  do t=1,ndof
     do c=1,ndof
        do l=1,ndof
           do k=kmin_,kmax_
              do j=jmin_,jmax_
                 do i=imin_,imax_

                    ! U Convective Term: -u*du/dx-v*du/dy-w*du/dz
                    v_here=0.25_WP * (v(i-1,j,k,c) + v(i,j,k,c) + v(i-1,j+1,k,c) + v(i,j+1,k,c))
                    w_here=0.25_WP * (w(i,j,k,c) + w(i-1,j,k,c) + w(i,j,k+1,c) + w(i-1,j,k+1,c))
                    rhsU(i,j,k,t)=rhsU(i,j,k,t) - (u(i,j,k,c) * (u(i+1,j,k,l) - u(i-1,j,k,l)) * 0.5_WP*dxi &
                                                  +v_here * (u(i,j+1,k,l) - u(i,j-1,k,l)) * 0.5_WP*dyi &
                                                  +w_here * (u(i,j,k+1,l) - u(i,j,k-1,l)) * 0.5_WP*dzi &
                                                  )*M(c,l,t)

                    ! V Convective Term: -u*dv/dx-v*dv/dy-w*dv/dz
                    u_here=0.25_WP * (u(i,j-1,k,c) + u(i+1,j-1,k,c) + u(i,j,k,c) + u(i+1,j,k,c))
                    w_here=0.25_WP * (w(i,j,k,c) + w(i,j,k+1,c) + w(i,j-1,k,c) + w(i,j-1,k+1,c))
                    rhsV(i,j,k,t)=rhsV(i,j,k,t) - (u_here * (v(i+1,j,k,l) - v(i-1,j,k,l)) * 0.5_WP * dxi &
                                                  +v(i,j,k,c) * (v(i,j+1,k,l) - v(i,j-1,k,l)) * 0.5_WP * dyi &
                                                  +w_here * (v(i,j,k+1,l) - v(i,j,k-1,l)) * 0.5_WP * dzi &
                                                  ) * M(c,l,t)

                    ! W Convective Term: -u*dw/dx-v*dw/dy-w*dw/dy
                    u_here=0.25_WP*(u(i,j,k,c) + u(i+1,j,k,c) + u(i+1,j,k-1,c) + u(i,j,k-1,c))
                    v_here=0.25_WP*(v(i,j,k,c) + v(i,j,k-1,c) + v(i,j+1,k-1,c) + v(i,j+1,k,c))
                    rhsW(i,j,k,t)=rhsW(i,j,k,t) - (u_here * (w(i+1,j,k,c) - w(i-1,j,k,c)) * 0.5_WP * dxi &
                            			     +v_here * (w(i,j+1,k,c) - w(i,j-1,k,c)) * 0.5_WP * dyi &
                                                  +w(i,j,k,c)*(w(i,j,k+1,c) - w(i,j,k-1,c)) * 0.5_WP * dzi &
                                                  ) * M(c,l,t)
                 end do
              end do
           end do
        end do
     end do

     do c=1,ndof_nu
        do l=1,ndof
           do k=kmin_,kmax_
              do j=jmin_,jmax_
                 do i=imin_,imax_

                    ! U Viscous Term
                    nu_pos_yx=0.25_WP*(nu(i  ,j  ,k, c)+nu(i  ,j+1,k, c) &
                              +nu(i-1,j  ,k, c)+nu(i-1,j+1,k, c) )

                    nu_neg_yx=0.25_WP*(nu(i  ,j  ,k ,c)+nu(i  ,j-1,k, c) &
                             +nu(i-1,j  ,k, c)+nu(i-1,j-1,k, c) )

                    nu_pos_zx=0.25_WP*(nu(i,j,k,c) + nu(i,j,k+1,c) + nu(i-1,j,k,c) + nu(i-1,j,k+1,c))

                    nu_neg_zx=0.25_WP*(nu(i,j,k,c) + nu(i-1,j,k,c) + nu(i,j,k-1,c) + nu(i-1,j,k-1,c))
                 
                    term1=nu(i  ,j,k, c) * (u(i+1,j  ,k, l)-u(i  ,j  , k, l)) * dxi**2 &
                         -nu(i-1,j,k, c) * (u(i  ,j  ,k, l)-u(i-1,j  ,k, l)) * dxi**2

                    term2=nu_pos_yx * (u(i  ,j+1,k, l)-u(i  ,j  ,k, l)) * dyi**2 &
                         -nu_neg_yx * (u(i  ,j  ,k, l)-u(i  ,j-1,k, l)) * dyi**2

                    term3=nu_pos_yx * (v(i  ,j+1,k, l)-v(i-1,j+1,k, l)) * dxi*dyi &
                         -nu_neg_yx * (v(i  ,j  ,k, l)-v(i-1,j  ,k, l)) *dxi*dyi

                    term4=nu_pos_zx * (u(i,j,k+1,l) - u(i,j,k,l)) * dzi**2 &
                         -nu_neg_zx * (u(i,j,k,l) - u(i,j,k-1,l)) * dzi**2

                    term5=nu_pos_zx * (w(i,j,k+1,l) - w(i-1,j,k+1,l)) * dxi*dzi &
                         -nu_neg_zx * (w(i,j,k,l) - w(i-1,j,k,l)) * dxi*dzi


                    rhsU(i,j,k,t)=rhsU(i,j,k,t)+(2.0_WP*term1+term2+term3+term4+term5)*M(c,l,t)
                 
                    ! V Viscous Term
                    nu_pos_xy=0.25_WP*(nu(i  ,j  ,k, c)+nu(i+1,j  ,k,c) &
                              +nu(i  ,j-1,k,c)+nu(i+1,j-1,k,c) )

                    nu_neg_xy=0.25_WP*(nu(i  ,j  ,k,c)+nu(i  ,j-1,k,c) &
                              +nu(i-1,j  ,k,c)+nu(i-1,j-1,k,c) )

                    nu_pos_zy=0.25_WP*(nu(i,j,k,c) + nu(i,j,k+1,c) + nu(i,j-1,k,c) + nu(i,j-1,k+1,c))

                    nu_neg_zy=0.25_WP*(nu(i,j,k,c) + nu(i,j-1,k,c) + nu(i,j,k-1,c) + nu(i,j-1,k-1,c))
                 
                    term1=nu(i,j  ,k,c)*(v(i  ,j+1,k,l)-v(i  ,j  ,k,l))*dyi**2 &
                         -nu(i,j-1,k,c)*(v(i  ,j  ,k,l)-v(i  ,j-1,k,l))*dyi**2

                    term2=nu_pos_xy       *(v(i+1,j  ,k,l)-v(i  ,j  ,k,l))*dxi**2 &
                         -nu_neg_xy       *(v(i  ,j  ,k,l)-v(i-1,j  ,k,l))*dxi**2

                    term3=nu_pos_xy       *(u(i+1,j  ,k,l)-u(i+1,j-1,k,l))*dyi*dxi &
                         -nu_neg_xy       *(u(i  ,j  ,k,l)-u(i  ,j-1,k,l))*dyi*dxi

                    term4=nu_pos_zy*(v(i,j,k+1,l) - v(i,j,k,l))*dzi**2 &
                         -nu_neg_zy*(v(i,j,k,l) - v(i,j,k-1,l))*dzi**2

                    term5=nu_pos_zy*(w(i,j,k+1,l) - w(i,j-1,k+1,l))*dzi*dyi &
                         -nu_neg_zy*(w(i,j,k,l) - w(i,j-1,k,l))*dzi*dyi



                    rhsV(i,j,k,t)=rhsV(i,j,k,t)+(2.0_WP*term1+term2+term3+term4+term5)*M(c,l,t)

                    ! W Viscous Term
                    nu_pos_xz=0.25_WP*(nu(i,j,k,c) + nu(i+1,j,k,c) + nu(i,j,k-1,c) + nu(i+1,j,k-1,c))

                    nu_neg_xz=0.25_WP*(nu(i,j,k,c) + nu(i,j,k-1,c) + nu(i-1,j,k,c) + nu(i-1,j,k-1,c))

                    nu_pos_yz=0.25_WP*(nu(i,j,k,c) + nu(i,j+1,k,c) + nu(i,j,k-1,c) + nu(i,j+1,k-1,c))

                    nu_neg_yz=0.25_WP*(nu(i,j,k,c) + nu(i,j-1,k,c) + nu(i,j,k-1,c) + nu(i,j-1,k-1,c))

                    term1=nu(i,j,k,c)*(w(i,j,k+1,l) - w(i,j,k,l))*dzi**2 &
                         -nu(i,j,k,c)*(w(i,j,k,l) - w(i,j,k-1,l))*dzi**2

                    term2=nu_pos_xz*(w(i+1,j,k,l) - w(i,j,k,l))*dxi**2 &
                         -nu_neg_xz*(w(i,j,k,l) - w(i-1,j,k,l))*dxi**2

                    term3=nu_pos_xz*(u(i+1,j,k,l) - u(i+1,j,k-1,l))*dzi*dxi &
                         -nu_neg_xz*(u(i,j,k,l) - u(i,j,k-1,l))*dzi*dxi

                    term4=nu_pos_yz*(w(i,j+1,k,l) - w(i,j,k,l))*dyi**2 &
                         -nu_neg_yz*(w(i,j,k,l) - w(i,j-1,k,l))*dyi**2

                    term5=nu_pos_yz*(v(i,j+1,k,l) - v(i,j+1,k-1,l))*dzi*dzi &
                         -nu_neg_yz*(v(i,j,k,l) - v(i,j,k-1,l))*dzi*dyi

                    rhsW(i,j,k,t)=rhsW(i,j,k,t)+(2.0_WP*term1+term2+term3+term4+term5)*M(c,l,t)

                 end do
              end do
           end do
        end do
     end do

  end do

end subroutine velocity_rhs

!==============================!
!     Velocity source term     !
!==============================!
subroutine velocity_source
  use velocity
  implicit none

  integer :: i,j,k
  
  sourceU=0.0_WP

  if (isxper) then ! ... calculate source term

     ! Calculate current mass flow rate
     mdot_local=0.0_WP
     do k = kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              mdot_local=mdot_local+0.5_WP*(u(i,j,k,1)+u(i+1,j,k,1))  !Source only modifies u velocity?
           end do
        end do
     end do
     call sum_real(mdot_local,mdot)

     ! Build source term based on difference 
     ! between initial and current   
     sourceU(1)=(mdot_init-mdot)/(nx*ny*nz)*1_WP  !Why is this divided by area/volume?
  end if
  
  return
end subroutine velocity_source

subroutine velocity_source_init
  use velocity
  implicit none

  integer :: i,j,k

  ! Calculate initial mass flow rate
  mdot_local=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           mdot_local=mdot_local+0.5_WP*(u(i,j,k,1)+u(i+1,j,k,1))
        end do
     end do
  end do

  call sum_real(mdot_local,mdot_init)
end subroutine velocity_source_init

!==============================!
!      Velocity divergence     !
!==============================!
subroutine velocity_divergence
  use velocity
  implicit none

  integer :: i,j,k
  double precision :: int, int_local
  double precision :: div_local

  div_local=0.0_WP
  do k=kmin_,kmax_
     do j=jmin_,jmax_
        do i=imin_,imax_
           div_local=div_local+((u(i+1,j,k,1)-u(i,j,k,1))*dxi+(v(i,j+1,k,1)-v(i,j,k,1))*dyi+(w(i,j,k+1,1)-w(i,j,k,1))*dzi)*dx*dy*dz
        end do
     end do
  end do

  call sum_real(div_local,vel_divergence)
  
  return
end subroutine velocity_divergence
