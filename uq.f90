! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module uq
  use variables
  use basis
  use grid
  implicit none

end module uq

!=========================!
!      Initialization     !
!=========================!
subroutine uq_init
  use uq
  use io
  implicit none

  
  ! Initialize viscosity
  if (is_uq) then
     call read_input('Uncertain Magnitude',sigma)
     allocate(nu(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof_nu))
     call uq_viscosity
     allocate(var_u(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
     allocate(var_v(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
     allocate(var_w(imin_:imax_,jmin_:jmax_,kmin_:kmax_))
  else
     ndof=1
     ndof_nu=1
     allocate(nu(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof_nu))
     nu=nu_io
     print *,'set nu'
     print *,maxval(nu)
  end if

  return
end subroutine uq_init


!=========================!
! Create random viscosity !
!=========================!
subroutine uq_viscosity
  use uq
  use math
  implicit none

  integer :: i,j,k,t   !might need k
  double precision :: x_here, y_here, z_here ![-1:1]

  ! Set viscosity based on test case
  select case(trim(simu))
     
  case ('poiseuile')
     nu=0.0_WP
     nu(:,:,:,1)=nu_io
     nu(:,:,:,2)=sigma
     
  case ('cylinder','square')
     ! nu(x,y,z)=nu_io + sum_k=1^d (sigma*cos(2*pi*k*x)*y_d(\omega))
     nu=0.0_WP
     nu(:,:,:,1)=nu_io
     do t=2,ndims+1
        do k=kmino_,kmaxo_
           do j=jmino_,jmaxo_
              do i=imino_,imaxo_
                 x_here=(xm(i)-(xmax+xmin)*0.5_WP)/(0.5_WP*(xmax-xmin))
                 y_here=(ym(j)-(ymax+ymin)*0.5_WP)/(0.5_WP*(ymax-ymin))
                 z_here=(zm(k)-(zmax+zmin)*0.5_WP)/(0.5_WP*(zmax-zmin))
                 nu(i,j,k,t)=sigma*cos(2*pi*(t-1)*y_here)*sin(2*pi*(t-1)*x_here)*cos(2*pi*(t-1)*z_here)/((t-1)**2*pi**2)
              end do
           end do
        end do
     end do
     
  case ('uncertainvelocity')
     nu=0.0_WP
     nu(:,:,:,1)=nu_io

  case default
     call die('Unknown simulation: '//trim(simu),' in uq_viscosity')
  end select
  
  return
end subroutine uq_viscosity

