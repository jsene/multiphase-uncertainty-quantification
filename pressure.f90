! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module pressure
  use grid
  use variables
  use math
  use basis
  implicit none

  double precision, dimension(:,:,:), pointer :: b
  double precision, dimension(:,:,:), pointer :: ro
  double precision, dimension(:,:,:), pointer :: q
  double precision, dimension(:,:,:), pointer :: s
  double precision, dimension(:,:,:), pointer :: t
  double precision, dimension(:,:,:), pointer :: n

  integer :: pressure_steps_max
  double precision :: p_residual_max

  contains 
    !=======================================!
    ! Operator L(f) : computes Laplacian(f) ! 
    !=======================================!
    function L(f)
      use grid
      implicit none
      
      ! Inputs
      double precision, dimension(nxo_,nyo_,nzo_) :: f

      ! Variables
      integer :: i,j,k

      ! Outputs
      double precision, dimension(nxo_,nyo_,nzo_) :: L

      ! Zero L
      L=0.0_WP

      ! Apply Neuman condition
      if (rankx.eq.0   ) f(nghost,:,:)       =f(1+nghost,:,:)
      if (rankx.eq.px-1) f(nxo_-nghost+1,:,:)=f(nxo_-nghost,:,:)
      if (ranky.eq.0   ) f(:,nghost,:)       =f(:,1+nghost,:)
      if (ranky.eq.py-1) f(:,nyo_-nghost+1,:)=f(:,nyo_-nghost,:)
      if (rankz.eq.0   ) f(:,:,nghost)       =f(:,:,1+nghost)
      if (rankz.eq.pz-1) f(:,:,nzo_-nghost+1)=f(:,:,nzo_-nghost)

      ! Calculate laplacian of A on interior of domain
      do k=1+nghost,nzo_-nghost
         do j=1+nghost,nyo_-nghost
            do i=1+nghost,nxo_-nghost
               L(i,j,k)=(-f(i-1,j  ,k  )+2.0_WP*f(i,j,k)-f(i+1,j  ,k  ))*dxi**2 &     
                       +(-f(i  ,j-1,k  )+2.0_WP*f(i,j,k)-f(i  ,j+1,k  ))*dyi**2 &  
                       +(-f(i  ,j  ,k-1)+2.0_WP*f(i,j,k)-f(i  ,j  ,k+1))*dzi**2  
            end do
         end do
      end do

      return
    end function L

end module pressure

!==============================!
!       Initialization         !
!==============================!
subroutine pressure_init
  use pressure
  implicit none

  ! Allocate data
  allocate(b (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(r (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(ro(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(q (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(s (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(t (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(n (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_))
  allocate(p (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))

  ! Inialize Pressure
  p=0.0_WP

  return
end subroutine pressure_init

!==============================!
! Calculate corrector pressure !
! laplacian(x)=div(U)          !
!==============================!
subroutine pressure_calc
  use pressure
  use io
  use communication
  use parallel
  implicit none

  integer :: i,j,k,d
  double precision :: int, int_local
  double precision :: inflow, outflow, scale

  ! Zero stats
  pressure_steps_max=0
  p_residual_max=0.0_WP

  ! Solve ndof decoupled Poisson equations
  do d=1,ndof

     ! Enforce divergence free condition
     ! Get inflow integral
     int_local=0.0_WP
     if (rankx.eq.0) then
        do k=kmin_,kmax_                   
           do j=jmin_,jmax_   
              int_local=int_local+u(imin,j,k,d)
           end do
        end do
     end if
     call sum_real(int_local,inflow)

     ! Get outflow integral
     int_local=0.0_WP
     if (rankx.eq.px-1) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              int_local=int_local+u(imax+1,j,k,d)
           end do
        end do
     end if
     call sum_real(int_local,outflow)

     ! Scale outflow
     scale=(inflow-outflow)/(ny*nz)
     if (rankx.eq.px-1) then
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              u(imax+1,j,k,d)=u(imax+1,j,k,d)+scale
           end do
        end do
     end if
     
     ! Form RHS
     b=0.0_WP
     do k=kmin_,kmax_
        do j=jmin_,jmax_
           do i=imin_,imax_
              b(i,j,k)=-rho/dt*((u(i+1,j,k,d)-u(i,j,k,d))*dxi+(v(i,j+1,k,d)-v(i,j,k,d))*dyi+(w(i,j,k+1,d)-w(i,j,k,d))*dzi)
           end do
        end do
     end do

     call comm_borders_2d(b)  

     ! Solve pressure equation using conjugate gradient
     call pressure_bicgstab(d)
     !call pressure_cg(d)

     ! Update stats
     pressure_steps_max=max(pressure_steps_max,pressure_steps)
     p_residual_max=max(p_residual_max,p_res)

  end do

  ! Update final stats
  pressure_steps=pressure_steps_max
  p_res=p_residual_max

  ! Communicate pressure
  call comm_borders(p)

  ! Apply Neuman conditions
  if (rankx.eq.0   ) p(imin-1,:,:,:)=p(imin,:,:,:)   
  if (rankx.eq.px-1) p(imax+1,:,:,:)=p(imax,:,:,:)
  if (ranky.eq.0   ) p(:,jmin-1,:,:)=p(:,jmin,:,:)
  if (ranky.eq.py-1) p(:,jmax+1,:,:)=p(:,jmax,:,:)
  if (rankz.eq.0   ) p(:,:,kmin-1,:)=p(:,:,kmin,:)
  if (rankz.eq.pz-1) p(:,:,kmax+1,:)=p(:,:,kmax,:)

  return
end subroutine pressure_calc

!===============================!
! Stabilized Conjugate Gradient !
!===============================!
subroutine pressure_bicgstab(d)
  use pressure
  use io
  use communication
  implicit none

  integer, intent(in) :: d

  double precision :: rhnew, rhold, alpha, omega,beta
  
  ! Initialize variables
  p(:,:,:,d)=0.0_WP
  ro   =b ! =b-L(p)
  r    =ro
  rhold=1.0_WP
  alpha=1.0_WP
  omega=1.0_WP
  q    =0.0_WP
  n    =0.0_WP
  

  pressure_steps=0
  p_res=sqrt(AtB(r,r))*dx*dy*dz

  do while (p_res.gt.cvg_criteria.and.pressure_steps.lt.pressure_max_steps)
     pressure_steps=pressure_steps+1
     rhnew=AtB(ro,r)
     beta=(rhnew/rhold)*(alpha/omega)
     q=ApBC(r,beta,ApBC(q,-1.0_WP*omega,n))
     call comm_borders_2d(q)
     n=L(q)   
     alpha=AtB(ro,n)
     alpha=rhnew/(sign(1.0_WP,alpha)*max(abs(alpha),epsilon(0.0_WP)))
     s=ApBC(r,-1.0_WP*alpha,n)
     call comm_borders_2d(s)
     t=L(s)
     omega=AtB(t,t)
     omega=AtB(t,s)/(sign(1.0_WP,omega)*max(abs(omega),epsilon(0.0_WP)))
     p(:,:,:,d)=ApBC(ApBC(p(:,:,:,d),alpha,q),omega,s)
     r=ApBC(s,-1.0_WP*omega,t)
     rhold=rhnew
     p_res=sqrt(AtB(r,r))*dx*dy*dz
  end do

end subroutine pressure_bicgstab

!===============================!
!       Conjugate Gradient      !
!===============================!
subroutine pressure_cg(d)
  use pressure
  use io
  use communication
  implicit none

  integer, intent(in) :: d

  double precision :: rh, rhi, alpha, beta, mu
  
  ! Initialize variables
  p(:,:,:,d)=0.0_WP
  r    =b ! =b-L(p)
  q    =0.0_WP
  t    =0.0_WP
  beta =0.0_WP
  s    =0.0_WP
  s    =L(r)
  rh   =AtB(r,r)
  rhi  =1.0_WP/rh
  mu   =AtB(s,r)
  alpha=rh/mu

  pressure_steps=0
  p_res=sqrt(rh)*dx*dy*dz

  do while (p_res.gt.cvg_criteria.and.pressure_steps.lt.pressure_max_steps)
     pressure_steps=pressure_steps+1
     t=ApBC(r,beta,t)
     q=ApBC(s,beta,q)
     p(:,:,:,d)=ApBC(p(:,:,:,d),alpha,t)
     r=ApBC(r,-1.0_WP*alpha,q)
     call comm_borders_2d(r)
     s=L(r)
     rh=AtB(r,r)
     p_res=sqrt(rh)*dx*dy*dz
     mu=AtB(s,r)               
     beta=rh*rhi
     rhi=1.0_WP/rh
     alpha=rh/(mu-rh*beta/alpha)
  end do

end subroutine pressure_cg
