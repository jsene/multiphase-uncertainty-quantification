! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

program main
  implicit none

  call initialize
  call core
  call finalize

end program main

!=========================!
!        Initialize       !
!=========================!
subroutine initialize
  use variables
  use grid
  use io
  use basis
  implicit none

  ! Initialize time
  time=0.0_WP

  ! Initialize iteration counter
  iter=0

  ! Read inputs file 
  call io_init

  ! Initialize core variables
  call variables_init

  ! Setup parallel environment
  call parallel_init
  
  ! Setup grid
  call grid_create

  ! Initialize basis
  call basis_init

  ! Initialize uncertainty quantification
  call uq_init

  ! Initialize velocity update
  call velocity_init

  ! Initialize simulation
  call simulation_init

  ! Initialize pressure solver
  call pressure_init

  ! Initialize visit and save first data set
  call visit_init
  call visit_data

  ! Initialize source term
  call velocity_source_init

  ! Write simulation summary
  if (rank.eq.root) then
     print*, '================================================='
     print*, '|            Simulation Parameters              |'
     print*, '================================================='
     print*, 'Simulation=         ',trim(simu)
     print*, 'Density=            ',rho
     print*, 'CFL=                ',CFL
     print*, 'Grid points=        ',nx,'X',ny,'Y',nz,'Z'
     if (is_uq) then
        print*, 'Degrees of freedom= ',ndof
        print*, 'Nu dof=             ',ndof_nu
     end if
     print*, '================================================='
  end if
  
  ! Intialize std-out
  if (rank.eq.root) then
     write (*,'(A)') '  Step       Time           max(U)         max(V)         max(W)        P residual  Steps    Divergence'
  end if


end subroutine initialize
!=========================!
!       Core Program      !
!=========================!
subroutine core
  use variables
  implicit none

  ! Start timers
  call timing_start(1)
  call timing_pause(1)
  call timing_start(2)
  call timing_pause(2)
  call timing_start(3)

  do while (.not.isdone)

     ! Write iteration to std-out
     call write_status

     ! Update counter
     iter=iter+1

     ! Update solution
     call simulation_step

     ! Update time
     time=time+dt

     ! Save solution to a file
     call visit_data

     ! Check if done
     call check_done
    
  end do

  ! Finish Timing
  call timing_end(3)
  call timing_resume(1)
  call timing_end(1)
  call timing_resume(2)
  call timing_end(2)
  call timing_output

end subroutine core

!=========================!
!         Finalize        !
!=========================!
subroutine finalize
  use variables
  implicit none

  ! Save final solution to a file
  iter=iter+1
  call visit_data

  ! Close parallel environment
  call parallel_final

end subroutine finalize


!=========================!
!      Check if done      !
!=========================!
subroutine check_done
  use variables
  use parallel
  implicit none

  if ( iter.ge.niter-1) then

     isdone=.true. 
     
     ! Update iteration count
     iter=iter+1

     ! Write iteration to std-out
     call write_status
     if (rank.eq.root) print *, 'Simulation complete!'

  end if
  
  return
end subroutine check_done

!=========================!
! Write status to std-out !
!=========================!
subroutine write_status
  use variables
  use parallel
  implicit none

  if (rank.ne.root) return

  write (*,'(I6,5ES15.5,I6,1ES15.5)') iter, time, &
       maxval(abs(u)), maxval(abs(v)), maxval(abs(w)), &
       p_res, pressure_steps, vel_divergence

  return
end subroutine write_status


!=========================!
!        Call die         !
!=========================!
subroutine die(text)
  use parallel
  implicit none

  character(len=*), intent(in) :: text
  print *, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
  print *, ' ', trim(text)
  print *, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
  call mpi_abort(MPI_COMM_WORLD,ierr)

end subroutine die
