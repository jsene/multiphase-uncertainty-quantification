! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!=========================!
!  Parallel environment   !
!=========================!
module parallel
  implicit none
  include 'mpif.h'

  integer :: rank, rankx, ranky, rankz, nproc
  integer, dimension(5) :: request_send
  integer, dimension(5) :: request_recv
  integer :: ierr, tag, comm, comm_3D, status(mpi_status_size)

  integer, parameter :: root=0

  integer :: px, py, pz

end module parallel

!=========================!
!        Initialize       !
!=========================!
subroutine parallel_init
  use parallel
  implicit none

  ! Initialize parallel environment
  call mpi_init(ierr)
  ! Duplicate communicator
  call mpi_comm_dup(mpi_comm_world,comm,ierr)

  ! Get size and rank(id #)
  call mpi_comm_size(comm, nproc, ierr)
  call mpi_comm_rank(comm, rank, ierr)

  call parallel_cartesian

end subroutine parallel_init

!=========================!
!         Finalize        !
!=========================!
subroutine parallel_final
  use parallel
  implicit none

  ! Finalize parallel environment
  call mpi_finalize(ierr)

end subroutine parallel_final

!=========================!
! Cartesian proc geometry !
!=========================!
subroutine parallel_cartesian
  use variables
  use parallel
  use io
  implicit none
  integer, dimension(3) :: dims
  logical, dimension(3) :: peri
  integer, dimension(3) :: coords

  call read_input('Processors in x',px)
  call read_input('Processors in y',py)
  call read_input('Processors in z',pz)

  dims(1)=px
  dims(2)=py
  dims(3)=pz

  peri=.false.
  if (isxper) peri(1)=.true.
  if (isyper) peri(2)=.true.
  if (iszper) peri(3)=.true.

  ! Check for correct number of procs 
  if (px*py*pz.ne.nproc) then
     call die("Wrong number of processors")
  end if

  ! Create caratesian grid of processors
  call mpi_cart_create(comm,3,dims,peri,1,comm_3D,ierr)
  call mpi_cart_coords(comm_3D,rank,3,coords,ierr)
  rankx=coords(1)
  ranky=coords(2)
  rankz=coords(3)

end subroutine parallel_cartesian
