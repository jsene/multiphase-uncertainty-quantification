! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module levelset
  use grid
  use variables
  use basis
  use communication
  implicit none

  logical :: use_multiphase
  double precision :: epsG

end module levelset

!==============================!
!        Initialization        !
!==============================!
subroutine levelset_init
  use levelset
  use io
  implicit none
  
  character(len=100) :: Gshape
  integer :: i,j,k
  double precision :: xo,yo,D

  ! Check if using multiphase
  call read_input('Use multiphase',use_multiphase)
  if (.not.use_multiphase) return

  ! Read interface shape from input file
  call read_input('Level set shape',Gshape)  

  ! Allocate level set
  allocate(G      (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(rhsG   (imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))
  allocate(rhsGold(imino_:imaxo_,jmino_:jmaxo_,kmino_:kmaxo_,1:ndof))

  ! Create initial level set - signed distance function
  select case(trim(Gshape))
  case ('Circle')
     call read_input('Circle xo',xo)
     call read_input('Circle yo',yo)
     call read_input('Circle D',D)
     G=0.0_WP
     ! Create distance level set
     do j=jmin_,jmax_
        do i=imin_,imax_
           G(i,j,:,1)=0.5_WP*D-sqrt((xm(i)-xo)**2+(ym(j)-yo)**2)
        end do
     end do
  case default
     call die('Unknown level set shape: '//trim(Gshape))
  end select

  ! Convert distance to hyperbolic tangent
  epsG=1.0_WP*(max(dx,dy))
  do j=jmin_,jmax_
     do i=imin_,imax_
        G(i,j,:,1)=0.5_WP*(tanh(G(i,j,:,1)/(2.0_WP*epsG))+1.0_WP)
     end do
  end do
  
  return
end subroutine levelset_init

!==============================!
!      Level Set Transport     !
!==============================!
subroutine levelset_step
  use levelset
  implicit none
  
  integer :: i,j,k,t

  ! Check is using multiphase
  if (.not.use_multiphase) return

  ! Calculate rhs
  call levelset_rhs

  if (iter.eq.1) then
     ! Update level set using explict Euler on first step
     do t=1,ndof
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 G(i,j,k,t)=G(i,j,k,t)+dt*rhsG(i,j,k,t)*mask(i,j,k)
                 ! save rhs
                 rhsGold(i,j,k,t)=rhsG(i,j,k,t)
              end do
           end do
        end do
     end do
  else
     ! Update using 2 step Adams-Bashforth
     do t=1,ndof
        do k=kmin_,kmax_
           do j=jmin_,jmax_
              do i=imin_,imax_
                 G(i,j,k,t)=G(i,j,k,t)+dt*(1.5_WP*rhsG(i,j,k,t)-0.5_WP*rhsUold(i,j,k,t))*mask(i,j,k)
                 ! Save old rhs
                 rhsGold(i,j,k,t)=rhsG(i,j,k,t)
              end do
           end do
        end do
     end do
  end if

  ! Renitialize level set
  call levelset_reinit
  
  return
end subroutine levelset_step

!==============================!
!    Level Set RHS - HOUC 3    !
!==============================!
subroutine levelset_rhs
  use levelset
  implicit none
  
  integer :: i,j,k,t,n,l
  real(WP) :: udGdx,vdGdy,wdGdz
  real(WP), parameter :: f1_6=0.16666666666666667_WP

  ! Zero rhs
  rhsG=0.0_WP

  ! Advection terms
  do t=1,ndof
     do n=1,ndof
        do l=1,ndof
           do k=kmin_,kmax_
              do j=jmin_,jmax_
                 do i=imin_,imax_
                    ! u*dG/dx
                    if (u(i,j,k,1).gt.0.0_WP) then
                       udGdx=u(i,j,k,n)*(+2.0_WP*G(i+1,j,k,l)+3.0_WP*G(i,j,k,l)-6.0_WP*G(i-1,j,k,l)+G(i-2,j,k,l))*f1_6*dxi
                    else 
                       udGdx=u(i,j,k,n)*(-2.0_WP*G(i-1,j,k,l)-3.0_WP*G(i,j,k,l)+6.0_WP*G(i+1,j,k,l)-G(i+2,j,k,l))*f1_6*dxi
                    end if
                    ! v*dG/dy
                    if (v(i,j,k,1).gt.0.0_WP) then
                       vdGdy=v(i,j,k,n)*(+2.0_WP*G(i,j+1,k,l)+3.0_WP*G(i,j,k,l)-6.0_WP*G(i,j-1,k,l)+G(i,j-2,k,l))*f1_6*dyi
                    else 
                       vdGdy=v(i,j,k,n)*(-2.0_WP*G(i,j-1,k,l)-3.0_WP*G(i,j,k,l)+6.0_WP*G(i,j+1,k,l)-G(i,j+2,k,l))*f1_6*dyi
                    end if
                    ! w*dG/dz
                    if (w(i,j,k,1).gt.0.0_WP) then
                       wdGdz=w(i,j,k,n)*(+2.0_WP*G(i,j,k+1,l)+3.0_WP*G(i,j,k,l)-6.0_WP*G(i,j,k-1,l)+G(i,j,k-2,l))*f1_6*dzi
                    else 
                       wdGdz=w(i,j,k,n)*(-2.0_WP*G(i,j,k-1,l)-3.0_WP*G(i,j,k,l)+6.0_WP*G(i,j,k+1,l)-G(i,j,k+2,l))*f1_6*dzi
                    end if
                    ! Add terms to RHS
                    rhsG(i,j,k,t)=rhsG(i,j,k,t)-(udGdx+vdGdy+wdGdz)*M(n,l,t)
                 end do
              end do
           end do
        end do
     end do
  end do

  return
end subroutine levelset_rhs

!==============================!
!  Level Set Reinitialization  !
!==============================!
subroutine levelset_reinit
  use levelset
  implicit none

  ! Compute interface normal vector
  call levelset_normal

  

  return
end subroutine levelset_reinit

!==============================!
!   Level Set Normal Vector    !
!==============================!
subroutine levelset_normal
  use levelset
  implicit none

  integer :: t,l,n
  real(WP), dimension(ndof) :: theta
  real(WP) :: G_here

  ! ! Project grad(G) onto theta using Gauss-Quadrature
  ! theta=0.0_WP
  ! do t=1,ndof
  !    ! Integrate
  !    do n=1,gauss_n
  !       G_here=0.0_WP
  !       do l=1,ndof
  !          G_here=G_here+G(l)*
  !          theta(t)=theta(t)+gauss_w(n)*atan2(G(i,j+1,...??
     
  !    ! Divide by <psi_t,psi_t>
  !    theta(t)=theta(t)/Var(t)
  ! end do

  

  return
end subroutine levelset_normal
