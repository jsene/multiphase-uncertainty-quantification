! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module timing
  use parallel
  implicit none

  double precision, dimension(3) :: start ! When current timer was started
  double precision, dimension(3) :: end   ! When current timer ends
  double precision, dimension(3) :: total ! Running sum of times

end module

!=========================!
!      Start timer        !
!=========================!
subroutine timing_start(n)
  use timing
  implicit none

  integer :: n

  ! Sync processors
  call mpi_barrier(comm_3D,ierr)

  ! Get start time
  start(n) = mpi_wtime()

  ! Zero total time
  total(n) = 0.0

  return
end subroutine timing_start

!=========================!
!       End timer         !
!=========================!
subroutine timing_end(n)
  use timing
  use communication
  implicit none

  integer :: n
  double precision :: tmp

  ! Sync processors
  call mpi_barrier(comm_3D,ierr)

  ! Get end time
  end(n) = mpi_wtime()
  
  ! Update total
  total(n) = total(n) + end(n)-start(n)

  ! Get wall time
  call max_real(total(n),tmp)
  total(n)=tmp

  return
end subroutine timing_end

!=========================!
!      Pause timer        !
!=========================!
subroutine timing_pause(n)
  use timing
  implicit none
  
  integer :: n

  ! Sync processors
  call mpi_barrier(comm_3D,ierr)

  ! Get end time
  end(n) = mpi_wtime()
  
  ! Update total
  total(n) = total(n) + end(n)-start(n)

  return
end subroutine timing_pause

!=========================!
!     Resume timer        !
!=========================!
subroutine timing_resume(n)
  use timing
  implicit none

  integer :: n

  ! Sync processors
  call mpi_barrier(comm_3D,ierr)
  
  ! Get new start time
  start(n) = mpi_wtime()  

  return
end subroutine timing_resume


!=========================!
!     Resume timer        !
!=========================!
subroutine timing_output
  use timing
  use parallel
  implicit none

  ! Print total time 
  if (rank.eq.root) then
     write (*,'(A,1E15.5,A)') 'Wall time for velocity   : ', total(1), ' seconds'
     write (*,'(A,1E15.5,A)') 'Wall time for pressure   : ', total(2), ' seconds'
     write (*,'(A,1E15.5,A)') 'Wall time for other      : ', total(3), ' seconds'
     write (*,'(A,1E15.5,A)') 'Wall time for everything : ', sum(total(:)), ' seconds'
  end if

  return
end subroutine timing_output
