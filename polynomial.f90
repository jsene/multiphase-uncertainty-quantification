! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

! ==================================================== !
! Framework for automagically manipulating polynomials !
! ==================================================== !
module polynomial
  use variables
  implicit none
  
  ! Data structure for polynomial: P(x)=sum{a(i)*x**p(i),i=1..n}
  type poly
     integer :: n
     double precision, dimension(:), pointer :: a
     integer,  dimension(:), pointer :: p
  endtype poly
  
contains
  
  ! =============================================== !
  ! Compute nth derivative of func, store it in der !
  ! =============================================== !
  subroutine derivative_n(func,der,n)
    implicit none
    
    type(poly), intent(in)  :: func
    type(poly), intent(out) :: der
    integer,    intent(in)  :: n
    
    integer :: i
    
    ! Count number of terms
    der%n=0
    do i=1,func%n
       if (func%p(i).ge.n) der%n=der%n+1
    end do
    
    ! Allocate der to correct size
    if (associated(der%a)) deallocate(der%a)
    if (associated(der%p)) deallocate(der%p)
    allocate(der%a(der%n))
    allocate(der%p(der%n))
    
    ! Take nth derivative of func
    der%n=0
    do i=1,func%n
       if (func%p(i).ge.n) then
          der%n=der%n+1
          der%a(der%n)=func%a(i)*real(factorial_nm(func%p(i),func%p(i)-n+1))
          der%p(der%n)=func%p(i)-n
       end if
    end do
    
    return
  end subroutine derivative_n
  
  
  ! =============================================== !
  ! Compute nth primitive of func, store it in prim !
  ! =============================================== !
  subroutine primitive_n(func,prim,n)
    implicit none
    
    type(poly), intent(in)  :: func
    type(poly), intent(out) :: prim
    integer,    intent(in)  :: n
    
    integer :: i,k
    
    ! Allocate prim to correct size
    prim%n=func%n
    if (associated(prim%a)) deallocate(prim%a)
    if (associated(prim%p)) deallocate(prim%p)
    allocate(prim%a(prim%n))
    allocate(prim%p(prim%n))
    
    ! Initialize prim to func
    prim%a=func%a
    prim%p=func%p
    
    ! Taking nth primitive of func
    do k=1,n
       do i=1,prim%n
          prim%a(i)=prim%a(i)/real(prim%p(i)+1)
          prim%p(i)=prim%p(i)+1
       end do
    end do
    
    return
  end subroutine primitive_n
  
  
  ! ========================================== !
  ! Compute integral of func between x1 and x2 !
  ! ========================================== !
  function integral(func,x1,x2)
    implicit none
    
    type(poly), intent(in) :: func
    double precision,   intent(in) :: x1
    double precision,   intent(in) :: x2
    double precision :: integral
    
    type(poly) :: prim
    
    ! Nullify
    nullify(prim%a)
    nullify(prim%p)
    
    ! Compute primitive of func
    call primitive_n(func,prim,1)
    
    ! Integrate
    integral=eval(prim,x2)-eval(prim,x1)
    
    return
  end function integral
  
  
  ! =========================== !
  ! Evaluate func at location x !
  ! =========================== !
  function eval(func,x)
    implicit none
    
    type(poly), intent(in) :: func
    double precision,   intent(in) :: x
    double precision :: eval
    
    integer :: i
    
    ! Evaluate polynomial
    eval=0.0_WP
    do i=1,func%n
       eval=eval+func%a(i)*x**func%p(i)
    end do
    
    return
  end function eval
  
  
  ! ======================== !
  ! Factorial: n*(n-1)*...*m !
  ! Note: n.ge.m is expected !
  ! ======================== !
  function factorial_nm(n,m)  
    implicit none
    
    integer, intent(in) :: n
    integer, intent(in) :: m
    integer :: factorial_nm
    
    integer :: i
    
    ! Evaluate factorial
    factorial_nm=1
    do i=m,n
       factorial_nm=factorial_nm*i
    end do
    
  end function factorial_nm
  
  
  ! ========================================== !
  ! Binomial coefficient: C(n,k)=n!/(k!(n-k)!) !
  ! ========================================== !
  function binomial(n,k)
    implicit none
    
    integer, intent(in) :: n
    integer, intent(in) :: k
    integer :: binomial
    
    ! Evaluate binomial coefficient
    binomial=factorial_nm(n,1)/(factorial_nm(k,1)*factorial_nm(n-k,1))
    
  end function binomial
  
end module polynomial
